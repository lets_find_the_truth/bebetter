//
//  BookService.swift
//  BeBetter
//
//  Created by Kyryl Nevedrov on 29/11/2019.
//  Copyright © 2019 Kyryl Nevedrov. All rights reserved.
//

import RxSwift
import RxRealm
import RealmSwift

struct BookService {
    private let realm = try! Realm()
    
    init() {
        self.createSomeBooks()
    }
    
    private func createSomeBooks() {
        guard realm.objects(Book.self).count == 0 else { return }
        
        let books = [
            Book(title: "My hero", isLiked: true),
            Book(title: "Harry Potter", isLiked: false),
            Book(title: "iOS Development", isLiked: true),
            Book(title: "UISwift", isLiked: false),
            Book(title: "Mark Twain", isLiked: false),
            Book(title: "Poppy lolly", isLiked: false),
            Book(title: "Zoooo", isLiked: false)
        ]
        books.forEach { book in
            try! realm.write {
                realm.add(book)
            }
        }
    }
    
    func like(_ book: Book) {
        try! realm.write {
            book.isLiked = true
        }
    }
    
    func unlike(_ book: Book) {
        try! realm.write {
            book.isLiked = false
        }
    }
    
    func getBooks() -> Observable<([Book], RealmChangeset?)> {
        return Observable.arrayWithChangeset(from: realm.objects(Book.self))
    }
    
    func delete(_ book: Book) {
        try! realm.write {
            realm.delete(book)
        }
    }
    
    func createRandomBook() {
        let book = Book(title: "Random \(Int.random(in: 0...100))", isLiked: false)
        let realm = try! Realm()
        try! realm.write {
            realm.add(book)
        }
    }
}
