//
//  PersonService.swift
//  BeBetter
//
//  Created by Kyryl Nevedrov on 28/10/2019.
//  Copyright © 2019 Kyryl Nevedrov. All rights reserved.
//

import Foundation
import Alamofire

struct LoginModel: Encodable {
    let email: String
    let password: String
}

enum PersonRequest: URLRequestModel {
    case create(LoginModel)
    case get

    // MARK: - Path
    internal var path: String {
        switch self {
        case .create:
            return "/person"
        case .get:
            return "/get"
        }
    }

    // MARK: - Parameters
    internal var parameters: Parameters? {
        switch self {
        case .create(let loginModel):
            return loginModel.dictionary
        default:
            return [
                "Tom": 12,
                "sdfd": "ssdfs"
                ]
        }
    }
    
    // MARK: - Methods
    internal var method: HTTPMethod {
        switch self {
        case .create:
            return .post
        case .get:
            return .get
        }
    }
    
    var successCodes: [Int] {
        return Array(200..<400)
    }
}

