//
//  NetworkManager.swift
//  BeBetter
//
//  Created by Kyryl Nevedrov on 29/10/2019.
//  Copyright © 2019 Kyryl Nevedrov. All rights reserved.
//

import Foundation
import Alamofire
import RxSwift

class NetworkManager {
    static let shared = NetworkManager()

    private var session: Alamofire.Session!

    private init() {
        let configuration = URLSessionConfiguration.default
        configuration.timeoutIntervalForRequest = 5
        self.session = Alamofire.Session(configuration: configuration)
    }
    
    // MARK: - Closures way
    
    func request<T: Decodable>(requestModel: URLRequestModel, completion: @escaping (RequestResult<T, AppError>) -> Void) {
        self.session
            .request(requestModel, interceptor: requestModel.interceptor)
            .validate(statusCode: requestModel.successCodes)
            .responseDecodable { (respose: DataResponse<T, AFError>) in
                if let error = respose.error {
                    let appError = AppError.network(error: error)
                    completion(RequestResult(value: nil, error: appError))
                } else {
                    completion(RequestResult(value: respose.value, error: nil))
                }
        }
    }
    
    func uploadRequest<T: Decodable>(requestModel: URLRequestModel,
                                     data: Data,
                                     name: String = "image",
                                     fileName: String = "image.jpg",
                                     mimeType: String = "image/jpeg",
                                     progressCompletion: @escaping (Double) -> Void,
                                     completion: @escaping (RequestResult<T, AppError>) -> Void) {
        self.session
            .upload(multipartFormData: { (multipartFormData) in
                multipartFormData.append(data, withName: name, fileName: fileName, mimeType: mimeType)
            }, with: requestModel,
               usingThreshold: UInt64.init(),
               interceptor: requestModel.interceptor)
            .validate(statusCode: requestModel.successCodes)
            .uploadProgress{ (progress) in
                progressCompletion(progress.fractionCompleted)
            }
            .responseDecodable { (respose: DataResponse<T, AFError>) in
                if let error = respose.error {
                    let appError = AppError.network(error: error)
                    completion(RequestResult(value: nil, error: appError))
                } else {
                    completion(RequestResult(value: respose.value, error: nil))
                }
        }
    }
    
    // MARK: - Reactive way
    
    func request<T: Decodable>(requestModel: URLRequestModel) -> Single<RequestResult<T, AppError>> {
        return Single.create { single in
            let request = self.session
                .request(requestModel, interceptor: requestModel.interceptor)
                .validate(statusCode: requestModel.successCodes)
                .responseDecodable { (respose: DataResponse<T, AFError>) in
                    debugPrint(respose)
                    if let value = respose.value {
                        single(.success(RequestResult(value: value, error: nil)))
                    } else if let error = respose.error {
                        let appError = AppError.network(error: error)
                        single(.success(RequestResult(value: nil, error: appError)))
                    }
            }
            
            return Disposables.create {
                request.cancel()
            }
        }
    }
    
    func uploadRequest<T: Decodable>(requestModel: URLRequestModel,
                                     data: Data,
                                     name: String = "image",
                                     fileName: String = "image.jpg",
                                     mimeType: String = "image/jpeg") -> Single<RequestResult<T, AppError>> {
        return Single.create { single in
            let request = self.session
                .upload(multipartFormData: { (multipartFormData) in
                    multipartFormData.append(data, withName: name, fileName: fileName, mimeType: mimeType)
                }, with: requestModel,
                   usingThreshold: UInt64.init(),
                   interceptor: requestModel.interceptor)
                .validate(statusCode: requestModel.successCodes)
                .responseDecodable { (respose: DataResponse<T, AFError>) in
                    if let value = respose.value {
                        single(.success(RequestResult(value: value, error: nil)))
                    } else if let error = respose.error {
                        let appError = AppError.network(error: error)
                        single(.success(RequestResult(value: nil, error: appError)))
                    }
            }
            
            return Disposables.create {
                request.cancel()
            }
        }
        
    }
}

struct EmptyResponse: Decodable {}

struct RequestResult<Success, Failure: Error> {
    var value: Success?
    var error: Failure?
}
