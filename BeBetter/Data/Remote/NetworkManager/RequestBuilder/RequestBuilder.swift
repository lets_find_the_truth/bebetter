//
//  RequestBuilder.swift
//  BeBetter
//
//  Created by Kyryl Nevedrov on 29/10/2019.
//  Copyright © 2019 Kyryl Nevedrov. All rights reserved.
//

import Foundation
import Alamofire

protocol URLRequestModel: URLRequestConvertible {
    // MARK: - Path
    var path: String { get }
    // MARK: - Parameters
    var parameters: Parameters? { get }
    // MARK: - Method
    var method: HTTPMethod { get }
    // MARK: - SuccessCodes
    var successCodes: [Int] { get }
    
    var mainURL: URL { get }
    var url: URL { get }
    var urlRequest: URLRequest { get }
    var encoding: ParameterEncoding { get }
    var interceptor: RequestInterceptor? { get }
}

extension URLRequestModel {
    var mainURL: URL {
        return URL(string: "https://httpbin.org")!
    }
    
    var url: URL {
        return mainURL.appendingPathComponent(path)
    }
    
    var urlRequest: URLRequest {
        var request = URLRequest(url: url)
        request.httpMethod = method.rawValue
        return request
    }

    var encoding: ParameterEncoding {
        switch method {
        case .get:
            return URLEncoding.default
        default:
            return JSONEncoding.default
        }
    }
    
    var interceptor: RequestInterceptor? {
        return Interceptor(adapter: RefreshTokenInterceptor(), retrier: RefreshTokenInterceptor())
    }
    
    // MARK: - URLRequestConvertible protocol functions

    func asURLRequest() throws -> URLRequest {
        return try encoding.encode(urlRequest, with: parameters)
    }
}
