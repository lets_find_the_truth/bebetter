//
//  RefreshTokenInterceptor.swift
//  BeBetter
//
//  Created by Kyryl Nevedrov on 31/10/2019.
//  Copyright © 2019 Kyryl Nevedrov. All rights reserved.
//

import Foundation
import Alamofire
import KeychainSwift

class RefreshTokenInterceptor: RequestAdapter, RequestRetrier {
    private typealias RefreshCompletion = (_ succeeded: Bool, _ accessToken: String?, _ refreshToken: String?) -> Void

    private let tokenIdentifier = "token"
    private let refreshTokenIdentifier = "token"
    private let keychain = KeychainSwift()
    private let lock = NSLock()
    private var isRefreshing = false
    private var requestsToRetry: [(RetryResult) -> Void] = []

    // MARK: - RequestAdapter

    func adapt(_ urlRequest: URLRequest, for session: Session, completion: @escaping (Result<URLRequest, Error>) -> Void) {
        var urlRequest = urlRequest
        if let accessToken = keychain.get(tokenIdentifier) {
            urlRequest.headers.update(.authorization(bearerToken: accessToken))
        }
        completion(.success(urlRequest))
    }

    // MARK: - RequestRetrier
    
    func retry(_ request: Request, for session: Session, dueTo error: Error, completion: @escaping (RetryResult) -> Void) {
        lock.lock() ; defer { lock.unlock() }

        if let response = request.task?.response as? HTTPURLResponse, response.statusCode == 401 {
            requestsToRetry.append(completion)

            if !isRefreshing {
                refreshTokens { [weak self] succeeded, accessToken, refreshToken in
                    guard let self = self else { return }

                    self.lock.lock() ; defer { self.lock.unlock() }

                    if let accessToken = accessToken, let refreshToken = refreshToken {
                        self.keychain.set(accessToken, forKey: self.tokenIdentifier)
                        self.keychain.set(refreshToken, forKey: self.refreshTokenIdentifier)
                    }

                    self.requestsToRetry.forEach { $0(succeeded ? .retry : .doNotRetry) }
                    self.requestsToRetry.removeAll()
                }
            }
        } else {
            completion(.doNotRetry)
        }
    }

    // MARK: - Private - Refresh Tokens

    private func refreshTokens(completion: @escaping RefreshCompletion) {
        guard !isRefreshing else { return }

        isRefreshing = true

        //TODO: refresh request
    }
}
