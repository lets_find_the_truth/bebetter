//
//  Book.swift
//  BeBetter
//
//  Created by Kyryl Nevedrov on 29/11/2019.
//  Copyright © 2019 Kyryl Nevedrov. All rights reserved.
//

import RealmSwift

class Book: Object {
    @objc dynamic var title: String = "unknown"
    @objc dynamic var isLiked: Bool = false
    
    convenience init(title: String, isLiked: Bool = false) {
        self.init()
        self.title = title
        self.isLiked = isLiked
    }
}
