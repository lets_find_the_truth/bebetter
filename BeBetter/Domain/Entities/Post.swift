//
//  PostCodable.swift
//  BeBetter
//
//  Created by Kyryl Nevedrov on 04/11/2019.
//  Copyright © 2019 Kyryl Nevedrov. All rights reserved.
//

import Foundation

// Codable usage
struct Post: Codable {
    var id: Int
    var firstName: String
    var comments: [Comments]
    
    enum CodingKeys: String, CodingKey {
        case id
        case firstName = "first_name"
        case comments
    }
}

struct Comments: Codable {
    var author: String
    var text: String
}
