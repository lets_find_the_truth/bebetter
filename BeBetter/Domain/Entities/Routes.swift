//
//  Routes.swift
//  BeBetter
//
//  Created by Kyryl Nevedrov on 06/11/2019.
//  Copyright © 2019 Kyryl Nevedrov. All rights reserved.
//

import Foundation

struct Routes: Codable {
    let status: String
    let routes: [Route]
}

struct Route: Codable {
    let overviewPolyline: RoutePoints
    
    enum CodingKeys: String, CodingKey {
        case overviewPolyline = "overview_polyline"
    }
}

struct RoutePoints: Codable {
    let points: String
}
