//
//  Comment.swift
//  BeBetter
//
//  Created by Kyryl Nevedrov on 26/11/2019.
//  Copyright © 2019 Kyryl Nevedrov. All rights reserved.
//

import Foundation

struct Comment: Codable {
    var name: String
    var body: String
}
