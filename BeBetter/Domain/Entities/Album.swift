//
//  Album.swift
//  BeBetter
//
//  Created by Kyryl Nevedrov on 10/11/2019.
//  Copyright © 2019 Kyryl Nevedrov. All rights reserved.
//

import Foundation
import Photos

class Album {
    var name: String
    var assets: [PHAsset]
    var collection: PHAssetCollection
    
    init(name: String?, assets: [PHAsset], collection: PHAssetCollection) {
        self.name = name ?? "unknown"
        self.assets = assets
        self.collection = collection
    }
}
