//
//  Person.swift
//  BeBetter
//
//  Created by Kyryl Nevedrov on 03/10/2019.
//  Copyright © 2019 Kyryl Nevedrov. All rights reserved.
//

import Foundation

final class Person {
    enum FollowingState: Int {
        case followed = 0
        case notConnected = 1
        case pending = 2
    }
    
    var name: String
    var followingState: FollowingState
    
    init(name: String, followingState: FollowingState) {
        self.name = name
        self.followingState = followingState
    }
}
