//
//  CommentsUseCase.swift
//  BeBetter
//
//  Created by Kyryl Nevedrov on 26/11/2019.
//  Copyright © 2019 Kyryl Nevedrov. All rights reserved.
//

import Foundation
import RxCocoa
import RxSwift

final class CommentsUseCase {
    func getComments(limit: Int, offset: Int, searchText: String) -> Single<RequestResult<[Comment], AppError>> {
        let commentsRequest = JSONPlaceholderService.getComments(limit: limit, offset: offset, searchText: searchText)
        return NetworkManager.shared.request(requestModel: commentsRequest)
    }
}
