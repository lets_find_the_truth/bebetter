//
//  LoginUseCase.swift
//  BeBetter
//
//  Created by Kyryl Nevedrov on 14/11/2019.
//  Copyright © 2019 Kyryl Nevedrov. All rights reserved.
//

import Foundation
import RxCocoa
import RxSwift

final class LoginUseCase {
    func login(name: String, pass: String) -> Single<RequestResult<Void, AppError>> {
        return Single.create { single in
            single(.success(RequestResult(value: nil, error: AppError.unknown(message: "Can't login"))))
            return Disposables.create()
        }
    }
}
