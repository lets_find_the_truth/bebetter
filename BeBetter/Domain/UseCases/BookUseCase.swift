//
//  BookUseCase.swift
//  BeBetter
//
//  Created by Kyryl Nevedrov on 29/11/2019.
//  Copyright © 2019 Kyryl Nevedrov. All rights reserved.
//

import Foundation
import RxCocoa
import RxSwift
import RxRealm

final class BookUseCase {
    let bookService = BookService()
    
    func like(_ book: Book) {
        self.bookService.like(book)
    }
    
    func unlike(_ book: Book) {
        self.bookService.unlike(book)
    }
    
    func getBooks() -> Observable<([Book], RealmChangeset?)> {
        return self.bookService.getBooks()
    }
    
    func delete(_ book: Book) {
        self.bookService.delete(book)
    }
    
    func createRandomBook() {
        return self.bookService.createRandomBook()
    }
}
