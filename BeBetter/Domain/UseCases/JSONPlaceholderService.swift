//
//  JSONPlaceholderService.swift
//  BeBetter
//
//  Created by Kyryl Nevedrov on 26/11/2019.
//  Copyright © 2019 Kyryl Nevedrov. All rights reserved.
//

import Foundation
import Alamofire
import MapKit

enum JSONPlaceholderService: URLRequestModel {
    case getComments(limit: Int, offset: Int, searchText: String)

    // MARK: - Path
    internal var path: String {
        switch self {
        case .getComments:
            return "/comments"
        }
    }

    // MARK: - Parameters
    internal var parameters: Parameters? {
        switch self {
        case .getComments(let limit, let offset, let searchText):
            return [
                "q": searchText,
                "_start": offset,
                "_limit": limit
            ]
        }
    }
    
    // MARK: - Methods
    internal var method: HTTPMethod {
        switch self {
        case .getComments:
            return .get
        }
    }
    
    internal var successCodes: [Int] {
        switch self {
        case .getComments:
            return [200]
        }
    }
    
    internal var mainURL: URL {
        return URL(string: "https://jsonplaceholder.typicode.com")!
    }
}
