//
//  RegistrationUseCase.swift
//  BeBetter
//
//  Created by Kyryl Nevedrov on 15/11/2019.
//  Copyright © 2019 Kyryl Nevedrov. All rights reserved.
//

import Foundation
import RxCocoa
import RxSwift

final class RegistrationUseCase {
    func registration(name: String, age: Int, pass: String) -> Single<RequestResult<Void, AppError>> {
        return Single.create { single in
            single(.success(RequestResult(value: (), error: nil)))
            return Disposables.create()
        }
    }
}
