//
//  ValidationUseCase.swift
//  BeBetter
//
//  Created by Kyryl Nevedrov on 15/11/2019.
//  Copyright © 2019 Kyryl Nevedrov. All rights reserved.
//

import Foundation
import RxCocoa
import RxSwift

final class ValidationUseCase {
    func validateName(name: String) -> Bool {
        let regEx = "[A-z]{3,50}"
        let testName = NSPredicate(format: "SELF MATCHES %@", regEx)
        return testName.evaluate(with: name)
    }
    
    func validatePass(pass: String) -> Bool {
        let regEx = ".{6,50}"
        let testName = NSPredicate(format: "SELF MATCHES %@", regEx)
        return testName.evaluate(with: pass)
    }
    
    func validateAge(age: Int) -> Bool {
        return age > 0 && age <= 100
    }
}
