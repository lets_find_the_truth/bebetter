//
//  RoundedImageView.swift
//  BeBetter
//
//  Created by Kyryl Nevedrov on 07/10/2019.
//  Copyright © 2019 Kyryl Nevedrov. All rights reserved.
//

import UIKit

@IBDesignable
class RoundedImageView: UIImageView {
    // MARK: - @IBInspactable
    @IBInspectable
    var isCircle: Bool = false
    
    @IBInspectable
    var cornerRadius: CGFloat = 0
    
    // MARK: - Init
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.clipsToBounds = true
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        self.clipsToBounds = true
    }
    
    // MARK: - Methods
    override func layoutSubviews() {
        self.clipsToBounds = true
        self.layer.cornerRadius = isCircle ? self.frame.height / 2 : self.cornerRadius
    }
}
