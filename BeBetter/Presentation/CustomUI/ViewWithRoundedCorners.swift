//
//  ViewWithRoundedCorners.swift
//  BeBetter
//
//  Created by Kyryl Nevedrov on 02/10/2019.
//  Copyright © 2019 Kyryl Nevedrov. All rights reserved.
//

import UIKit

@IBDesignable
class ViewWithRoundedCorners: UIView {
    // MARK: - @IBInspectables
    @IBInspectable
    var cornerRadius: CGFloat = 0 {
        didSet {
            self.clipsToBounds = true
            self.layer.cornerRadius = self.cornerRadius
        }
    }
}
