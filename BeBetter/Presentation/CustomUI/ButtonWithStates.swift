//
//  ButtonWithStates.swift
//  BeBetter
//
//  Created by Kyryl Nevedrov on 02/10/2019.
//  Copyright © 2019 Kyryl Nevedrov. All rights reserved.
//

import UIKit

@IBDesignable
class ButtonWithStates: UIButton {
    enum ButtonState: Int {
        case followed = 0
        case notConnected = 1
        case pending = 2
    }
    
    // MARK: - @IBInspectables
    @IBInspectable
    var buttonStateIndex: Int = 0 {
        willSet {
            self.buttonState = ButtonState.init(rawValue: self.buttonStateIndex) ?? .followed
        }
    }
    
    // MARK: - Properties
    var buttonState: ButtonState = .followed {
        didSet {
            self.configureUI(for: buttonState)
        }
    }
    
    // MARK: - Methods
    private func configureUI(for buttonState: ButtonState) {
        switch buttonState {
        case .followed:
            self.titleLabel?.font = UIFont(.gothamProBol(size: 12))
            self.setTitle("Followed", for: .normal)
        case .notConnected:
            self.titleLabel?.font = UIFont(.gothamProBol(size: 14))
            self.setTitle("Not Connected", for: .normal)
        case .pending:
            self.titleLabel?.font = UIFont(.gothamProMediumItalic(size: 10))
            self.setTitle("Pending...", for: .normal)
        }
    }
}
