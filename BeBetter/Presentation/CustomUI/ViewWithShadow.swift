//
//  ViewWithShadow.swift
//  BeBetter
//
//  Created by Kyryl Nevedrov on 02/10/2019.
//  Copyright © 2019 Kyryl Nevedrov. All rights reserved.
//

import UIKit

@IBDesignable
class ViewWithShadow: UIView {
    // MARK: - @IBInspectables
    @IBInspectable
    var shadowColor: UIColor = UIColor.black {
       didSet {
           self.layer.shadowColor = self.shadowColor.cgColor
       }
    }
    
    @IBInspectable
    var shadowRadius: CGFloat = 0 {
        didSet {
            self.layer.shadowRadius = self.shadowRadius
            self.layer.masksToBounds = false
            self.layer.shadowOpacity = 0.8
            self.layer.shouldRasterize = true
            self.layer.rasterizationScale = UIScreen.main.scale
        }
    }
    
    @IBInspectable
    var shadowOffset: CGSize = CGSize(width: 0, height: 0) {
        didSet {
            self.layer.shadowOffset = CGSize(width: 0, height: 6)
        }
    }
    
    @IBInspectable
    var cornerRadius: CGFloat = 0 {
        didSet {
            self.layer.cornerRadius = self.cornerRadius
        }
    }
    
    // MARK: - Methods
    override func layoutSubviews() {
        super.layoutSubviews()
        self.layer.shadowPath = UIBezierPath(rect: bounds).cgPath
    }
}
