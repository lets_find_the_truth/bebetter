//
//  ViewModelType.swift
//  BeBetter
//
//  Created by Kyryl Nevedrov on 18/11/2019.
//  Copyright © 2019 Kyryl Nevedrov. All rights reserved.
//

import Foundation

protocol ViewModel {
    associatedtype Input
    associatedtype Output

    var input: Input { get }
    var output: Output { get }
}
