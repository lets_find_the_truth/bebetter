//
//  TableViewCellProtocols.swift
//  BeBetter
//
//  Created by Kyryl Nevedrov on 04/10/2019.
//  Copyright © 2019 Kyryl Nevedrov. All rights reserved.
//

import Foundation

protocol CellWithViewModel {
    func configure<T: CellViewModel>(with viewModel: T)
}

protocol CellViewModel: ViewModel {}
