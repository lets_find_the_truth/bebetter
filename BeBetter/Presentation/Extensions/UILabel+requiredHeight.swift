//
//  UILabel+requiredHeight.swift
//  BeBetter
//
//  Created by Kyryl Nevedrov on 25/10/2019.
//  Copyright © 2019 Kyryl Nevedrov. All rights reserved.
//

import UIKit

extension UILabel {
    var requiredHeight: CGFloat {
        let label = UILabel(frame: CGRect(x: 0,
                                          y: 0,
                                          width: frame.width,
                                          height: CGFloat.greatestFiniteMagnitude))
        label.numberOfLines = 0
        label.font = font
        label.text = text
        label.attributedText = attributedText
        label.sizeToFit()
        return label.frame.height
    }
}
