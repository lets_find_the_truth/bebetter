//
//  UIScrollView+nearTheEdge.swift
//  BeBetter
//
//  Created by Kyryl Nevedrov on 26/11/2019.
//  Copyright © 2019 Kyryl Nevedrov. All rights reserved.
//

import UIKit

extension UIScrollView {
    func isNearBottomEdge(edgeOffset: CGFloat = 20.0) -> Bool {
        return self.contentOffset.y + self.frame.size.height + edgeOffset > self.contentSize.height
    }
}
