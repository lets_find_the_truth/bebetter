//
//  UIViewController+hideKeyboard.swift
//  BeBetter
//
//  Created by Kyryl Nevedrov on 26/10/2019.
//  Copyright © 2019 Kyryl Nevedrov. All rights reserved.
//

import UIKit

extension UIViewController {
    func hideKeyboardWhenTapOutsize() {
        let tap = UITapGestureRecognizer(target: self, action: #selector(hideKeyboard))
        self.view.addGestureRecognizer(tap)
    }
    
    @objc private func hideKeyboard() {
        self.view.endEditing(true)
    }
}
