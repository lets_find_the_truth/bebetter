//
//  DocumentPickerViewController+rxDidSelectDocument.swift
//  BeBetter
//
//  Created by Kyryl Nevedrov on 06/12/2019.
//  Copyright © 2019 Kyryl Nevedrov. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

class RxDocumentPickerViewControllerProxy: DelegateProxy<UIDocumentPickerViewController, UIDocumentPickerDelegate>, UIDocumentPickerDelegate, DelegateProxyType  {
    
    init(documentPicker: UIDocumentPickerViewController) {
        super.init(parentObject: documentPicker, delegateProxy: RxDocumentPickerViewControllerProxy.self)
    }
    
    static func currentDelegate(for object: UIDocumentPickerViewController) -> UIDocumentPickerDelegate? {
        return object.delegate
    }
    
    static func setCurrentDelegate(_ delegate: UIDocumentPickerDelegate?, to object: UIDocumentPickerViewController) {
        object.delegate = delegate
    }
    
    static func registerKnownImplementations() {
        self.register { RxDocumentPickerViewControllerProxy(documentPicker: $0) }
    }
}

extension Reactive where Base: UIDocumentPickerViewController {
    public var delegate: DelegateProxy<UIDocumentPickerViewController, UIDocumentPickerDelegate> {
        return RxDocumentPickerViewControllerProxy.proxy(for: base)
    }
    
    public var didPickDocumentAt: Observable<[URL]> {
        return delegate
            .methodInvoked(#selector(UIDocumentPickerDelegate.documentPicker(_:didPickDocumentsAt:)))
            .compactMap { values in
                return values[1] as? [URL]
            }
    }
}
