//
//  AVCaptureMetadataOutput+rxMetadataOutput.swift
//  BeBetter
//
//  Created by Kyryl Nevedrov on 07/12/2019.
//  Copyright © 2019 Kyryl Nevedrov. All rights reserved.
//

import Foundation
import AVFoundation
import RxSwift
import RxCocoa

class RxAVCaptureMetadataOutputProxy: DelegateProxy<AVCaptureMetadataOutput, AVCaptureMetadataOutputObjectsDelegate>, DelegateProxyType, AVCaptureMetadataOutputObjectsDelegate {

    init(matadataOutput: AVCaptureMetadataOutput) {
        super.init(parentObject: matadataOutput, delegateProxy: RxAVCaptureMetadataOutputProxy.self)
    }
    
    static func registerKnownImplementations() {
        self.register { RxAVCaptureMetadataOutputProxy(matadataOutput: $0) }
    }
    
    static func currentDelegate(for object: AVCaptureMetadataOutput) -> AVCaptureMetadataOutputObjectsDelegate? {
        return object.metadataObjectsDelegate
    }
    
    static func setCurrentDelegate(_ delegate: AVCaptureMetadataOutputObjectsDelegate?, to object: AVCaptureMetadataOutput) {
        object.setMetadataObjectsDelegate(delegate, queue: DispatchQueue.main)
    }
}

extension Reactive where Base: AVCaptureMetadataOutput {
    public var delegate: DelegateProxy<AVCaptureMetadataOutput, AVCaptureMetadataOutputObjectsDelegate> {
        return RxAVCaptureMetadataOutputProxy.proxy(for: base)
    }
    
    public var metadataOutput: Observable<[AVMetadataObject]> {
        return delegate
            .methodInvoked(#selector(AVCaptureMetadataOutputObjectsDelegate.metadataOutput(_:didOutput:from:)))
            .compactMap { values in
                return values[1] as? [AVMetadataObject]
            }
    }
}
