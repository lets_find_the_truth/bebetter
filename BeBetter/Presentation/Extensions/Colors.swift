//
//  Colors.swift
//  BeBetter
//
//  Created by Kyryl Nevedrov on 21/10/2019.
//  Copyright © 2019 Kyryl Nevedrov. All rights reserved.
//

import UIKit

extension UIColor {
    private enum Colors {
        static var green = UIColor(red: 0, green: 1, blue: 0, alpha: 1)
    }
    
    enum Separator {
        static var tableView = Colors.green
        static var collectionView = Colors.green
    }
    
    enum Label {
        static var title = UIColor.black
        static var description = Colors.green
    }
    
    enum Button {
        enum ActionButton {
            static var background = UIColor.black
            static var title = Colors.green
        }
        
        enum LoginButton {
            static var background = UIColor.black
            static var title = UIColor.white
        }
    }
}
