//
//  UIViewController+showAppError.swift
//  BeBetter
//
//  Created by Kyryl Nevedrov on 27/11/2019.
//  Copyright © 2019 Kyryl Nevedrov. All rights reserved.
//

import UIKit

extension UIViewController {
    func show(error: AppError) {
        let alertController = UIAlertController(title: "Error", message: error.localizedDescription, preferredStyle: .alert)
        alertController.addAction(UIAlertAction(title: "OK", style: .cancel, handler: nil))
        self.present(alertController, animated: true, completion: nil)
    }
}
