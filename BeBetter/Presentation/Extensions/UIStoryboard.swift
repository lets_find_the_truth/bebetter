//
//  UIStoryboard.swift
//  BeBetter
//
//  Created by Kyryl Nevedrov on 18/11/2019.
//  Copyright © 2019 Kyryl Nevedrov. All rights reserved.
//

import UIKit

extension UIStoryboard {
    enum StoryboardType: String {
        case Login
        case Registration
        case Picker
        case Home
    }
    
    convenience init(storyboardType: StoryboardType) {
        self.init(name: storyboardType.rawValue, bundle: .main)
    }
    
    func initFromStoryboard<T>(viewController: T.Type) -> T {
        let viewControllerIdentifier = String(describing: viewController)
        return self.instantiateViewController(withIdentifier: viewControllerIdentifier) as! T
    }
}
