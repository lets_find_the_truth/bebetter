//
//  UIViewController+statusAndNavBarHeight.swift
//  BeBetter
//
//  Created by Kyryl Nevedrov on 03/12/2019.
//  Copyright © 2019 Kyryl Nevedrov. All rights reserved.
//

import UIKit

extension UIViewController {
    var statusAndNavBarHeight: CGFloat {
        return UIApplication.shared.statusBarFrame.size.height +
            (navigationController?.navigationBar.frame.height ?? 0.0)
    }
}
