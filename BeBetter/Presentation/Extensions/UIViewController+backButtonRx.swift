//
//  UIViewController+backButtonRx.swift
//  BeBetter
//
//  Created by Kyryl Nevedrov on 25/11/2019.
//  Copyright © 2019 Kyryl Nevedrov. All rights reserved.
//

#if os(iOS) || os(tvOS)
import UIKit
import RxCocoa
import RxSwift

public extension Reactive where Base: UIViewController {
    //observable that triggered when user press back button
    var backButtonTapped: ControlEvent<Void> {
        let isMovingFromParent = self.base.isMovingFromParent
        let source = self.methodInvoked(#selector(Base.viewWillDisappear))
            .compactMap { _ -> Void? in
                guard !isMovingFromParent else { return nil }
                return ()
            }
        return ControlEvent(events: source)
    }
}
#endif
