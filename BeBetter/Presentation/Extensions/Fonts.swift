//
//  Fonts.swift
//  BeBetter
//
//  Created by Kyryl Nevedrov on 22/10/2019.
//  Copyright © 2019 Kyryl Nevedrov. All rights reserved.
//

import UIKit

//Scaled instance 
//label.font = UIFontMetrics.default.scaledFont(for: customFont)
//label.adjustsFontForContentSizeCategory = true

extension UIFont {
    enum FontType {
        case gothamProMediumItalic(size: CGFloat)
        case gothamProBol(size: CGFloat)
    }
    
    convenience init(_ fontType: FontType) {
        switch fontType {
        case let .gothamProMediumItalic(size):
            self.init(name: "GothamPro-MediumItalic", size: size)!
        case let .gothamProBol(size):
            self.init(name: "GothamPro-Bold", size: size)!
        }
    }
}
