//
//  GoogleMapViewController.swift
//  BeBetter
//
//  Created by Kyryl Nevedrov on 05/11/2019.
//  Copyright © 2019 Kyryl Nevedrov. All rights reserved.
//

import UIKit
import GoogleMaps
import RxSwift

class GoogleMapViewController: UIViewController {
    // MARK: - @IBOutlets
    @IBOutlet weak var mapView: GMSMapView!
    @IBOutlet weak var mapTypeSegmentedControl: UISegmentedControl!
    @IBOutlet weak var centerUserPositionButton: UIButton!
    @IBOutlet weak var routeToKharkivButton: UIButton!

    // MARK: - Properties
    private var disposeBag = DisposeBag()
    private let locationManager = LocationManager()
    private let kharkivCoordinate = CLLocationCoordinate2D(latitude: 49.988358,
                                                        longitude: 36.232845)
    // MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.configureUI()
        self.configureRX()
    }

    // MARK: - ConfigureUI
    private func configureUI() {
        self.mapView.isMyLocationEnabled = true
    }

    // MARK: - ConfigureRX
    private func configureRX() {
        self.locationManager.start()
        self.centerUserPositionButton.rx.tap
         .bind { [weak self] in self?.centerMapOnUserLocation()}
         .disposed(by: disposeBag)
        self.routeToKharkivButton.rx.tap
         .bind { [weak self] in self?.drawRouteToKharkiv()}
         .disposed(by: disposeBag)
        self.mapTypeSegmentedControl.rx.value
         .bind { [weak self] value in
            self?.mapView.mapType = GMSMapViewType.init(rawValue: UInt(value + 1)) ?? .normal
         }
         .disposed(by: disposeBag)
     
    }
     
    // MARK: - Methods
    private func centerMapOnUserLocation() {
        
        guard let currentLocation = locationManager.currentLocation.value else { return }
        
        //Zoom: 1: World 5: Landmass/continent 10: City 15: Streets 20: Buildings
        let camera = GMSCameraPosition(target: currentLocation.coordinate, zoom: 15)
        self.mapView.animate(to: camera)
    }
    
    private func drawRouteToKharkiv() {
        guard let currentLocation = locationManager.currentLocation.value else { return }
        self.mapView.clear()
        let marker = GMSMarker(position: self.kharkivCoordinate)
        marker.title = "Kharkiv"
        marker.snippet = "Population: 1 446 107"
        marker.infoWindowAnchor = CGPoint(x: 0.5, y: 0.2)
        let image = UIImage(named: "mapPinTree")
        marker.iconView = UIImageView(image: image)
        marker.map = mapView
        
        NetworkManager.shared.request(requestModel: MapService.getRoutes(source: kharkivCoordinate, destination: currentLocation.coordinate))
            .subscribe(onSuccess: { [weak self] (result: RequestResult<Routes, AppError>) in
                guard let routes = result.value,
                      let route = routes.routes.first else { return }
                self?.drawRoute(points: route.overviewPolyline.points)
            })
            .disposed(by: disposeBag)
    }
    
    private func drawRoute(points: String) {
        let route = GMSPath(fromEncodedPath: points)
        let polyline = GMSPolyline(path: route)
        polyline.strokeColor = UIColor.gray
        polyline.strokeWidth = 2
        polyline.map = self.mapView
    }
}

extension GoogleMapViewController: GMSMapViewDelegate {
    func mapView(_ mapView: GMSMapView, idleAt position: GMSCameraPosition) {
        print("idle at postion: \(position)")
    }
    
    func mapView(_ mapView: GMSMapView, didTapInfoWindowOf marker: GMSMarker) {
        print("didTpaInfoView of marker: \(marker)")
    }
}
