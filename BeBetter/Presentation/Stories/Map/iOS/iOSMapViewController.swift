//
//  iOSMapViewController.swift
//  BeBetter
//
//  Created by Kyryl Nevedrov on 05/11/2019.
//  Copyright © 2019 Kyryl Nevedrov. All rights reserved.
//

import UIKit
import MapKit
import RxSwift

class iOSMapViewController: UIViewController {
    // MARK: - @IBOutlets
    @IBOutlet weak var mapView: MKMapView!
    @IBOutlet weak var mapTypeSegmentedControl: UISegmentedControl!
    @IBOutlet weak var centerUserPositionButton: UIButton!
    @IBOutlet weak var routeToKharkivButton: UIButton!
    
    // MARK: - Properties
    private var disposeBag = DisposeBag()
    private let locationManager = LocationManager()
    private let kharkivCoordinate = CLLocationCoordinate2D(latitude: 49.988358,
                                                           longitude: 36.232845)
    // MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.configureUI()
        self.configureRX()
    }
    
    // MARK: - ConfigureUI
    private func configureUI() {
        self.mapView.showsUserLocation = true
    }

    // MARK: - ConfigureRX
    private func configureRX() {
        self.locationManager.start()
        self.centerUserPositionButton.rx.tap
            .bind { [weak self] in self?.centerMapOnUserLocation()}
            .disposed(by: disposeBag)
        self.routeToKharkivButton.rx.tap
            .bind { [weak self] in self?.drawRouteToKharkiv()}
            .disposed(by: disposeBag)
        self.mapTypeSegmentedControl.rx.value
            .bind { [weak self] value in
                self?.mapView.mapType = MKMapType.init(rawValue: UInt(value)) ?? MKMapType.standard
            }
            .disposed(by: disposeBag)
        
    }
    
    // MARK: - Methods
    private func centerMapOnUserLocation() {
        guard let currentLocation = locationManager.currentLocation.value else { return }
        let region = MKCoordinateRegion(center: currentLocation.coordinate, latitudinalMeters: 100, longitudinalMeters: 100)
        self.mapView.setRegion(region, animated: true)
    }
    
    private func drawRouteToKharkiv() {
        guard let currentLocation = locationManager.currentLocation.value else { return }
        let annotation = Pin(coordinate: self.kharkivCoordinate, title: "Kharkiv", subtitle: "Kharkiv city", type: .park)
        self.mapView.addAnnotation(annotation)
        
        let sourcePlacemark = MKPlacemark(coordinate: currentLocation.coordinate)
        let destinationPlacemark = MKPlacemark(coordinate: kharkivCoordinate)

        let directionsRequest = MKDirections.Request()
        directionsRequest.source = MKMapItem(placemark: sourcePlacemark)
        directionsRequest.destination = MKMapItem(placemark: destinationPlacemark)
        directionsRequest.transportType = .walking
        
        let directions = MKDirections(request: directionsRequest)
        directions.calculate { [weak self] (response, error) in
            guard let response = response, let mainRoute = response.routes.first else { return }
            self?.mapView.addOverlay(mainRoute.polyline, level: .aboveRoads)
        
            let routeRect = mainRoute.polyline.boundingMapRect
            self?.mapView.setVisibleMapRect(routeRect, animated: true)
        }
    }
}

extension iOSMapViewController: MKMapViewDelegate {
    func mapViewDidChangeVisibleRegion(_ mapView: MKMapView) {}
    
    func mapView(_ mapView: MKMapView, rendererFor overlay: MKOverlay) -> MKOverlayRenderer {
        let renderer = MKPolylineRenderer(overlay: overlay)
        renderer.strokeColor = UIColor.gray
        renderer.lineWidth = 2
        return renderer
    }
    
    func mapView(_ mapView: MKMapView, didSelect view: MKAnnotationView) {}
    
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        guard let pin = annotation as? Pin else {
            return nil
        }
        let pinViewIdentifier = "pinView"
        
        let pinView = mapView.dequeueReusableAnnotationView(withIdentifier: pinViewIdentifier) ?? MKAnnotationView(annotation: pin, reuseIdentifier: pinViewIdentifier)
        
        pinView.image = pin.image.resizeTo(newSize: CGSize(width: 50, height: 50))
        pinView.canShowCallout = true
        // add button to right accessory view (there are left and detail view too)
        pinView.rightCalloutAccessoryView = UIButton(type: .infoDark)
        // make pin dragable
        pinView.isDraggable = true
 
        return pinView
    }
    
    func mapView(_ mapView: MKMapView, annotationView view: MKAnnotationView, calloutAccessoryControlTapped control: UIControl) {}
}

fileprivate class Pin: NSObject, MKAnnotation {
    enum PinType {
        case bowling
        case park
    }
    
    var coordinate: CLLocationCoordinate2D
    var title: String?
    var subtitle: String?
    var image: UIImage
    
    init(coordinate: CLLocationCoordinate2D, title: String, subtitle: String, type: PinType) {
        self.coordinate = coordinate
        self.title = title
        self.subtitle = subtitle
        switch type {
        case .bowling:
            self.image = UIImage(named: "mapPinBowl")!
        case .park:
            self.image = UIImage(named: "mapPinTree")!
        }
    }

}
