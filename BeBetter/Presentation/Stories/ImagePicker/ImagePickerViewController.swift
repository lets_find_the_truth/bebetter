//
//  ImagePickerViewController.swift
//  BeBetter
//
//  Created by Kyryl Nevedrov on 11/11/2019.
//  Copyright © 2019 Kyryl Nevedrov. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

class ImagePickerViewController: UIViewController {
    // MARK: - @IBOutlets
    @IBOutlet var imageView: UIImageView!
    @IBOutlet var cameraButton: UIButton!
    @IBOutlet var galleryButton: UIButton!
    @IBOutlet var cropButton: UIButton!
    
    // MARK: - Properties
    private let bag = DisposeBag()
    var viewModel: ImagePickerViewModel! = ImagePickerViewModel()
    
    // MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.configureInputs()
        self.configureOutputs()
    }
    
    // MARK: - Configure Inputs
    private func configureInputs() {
        let output = self.viewModel.output
        output.image
            .drive(imageView.rx.image)
            .disposed(by: bag)
    }

    // MARK: - Configure Outputs
    private func configureOutputs() {
        let input = self.viewModel.input
        self.cameraButton.rx.tap
            .map { [weak self] _ in return self }
            .bind(to: input.cameraTrigger)
            .disposed(by: bag)
        self.galleryButton.rx.tap
             .map { [weak self] _ in return self }
             .bind(to: input.galleryTrigger)
             .disposed(by: bag)
        self.cropButton.rx.tap
             .map { [weak self] _ in return self }
             .bind(to: input.cropTrigger)
             .disposed(by: bag)
    }
}
