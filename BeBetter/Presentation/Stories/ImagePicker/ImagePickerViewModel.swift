//
//  ImagePickerViewModel.swift
//  BeBetter
//
//  Created by Kyryl Nevedrov on 07/12/2019.
//  Copyright © 2019 Kyryl Nevedrov. All rights reserved.
//

import RxSwift
import RxCocoa

final class ImagePickerViewModel: ViewModel {
    // MARK: - Properties
    var input: Input
    var output: Output
    
    // MARK: - Init
    init() {
        self.input = Input(cameraTrigger: PublishRelay<UIViewController?>(),
                           galleryTrigger: PublishRelay<UIViewController?>(),
                           cropTrigger: PublishRelay<UIViewController?>())
        
        
        
        
        let cameraImage = input.cameraTrigger
            .flatMapLatest { viewController in
                return UIImagePickerController.rx.createWithParent(viewController) { picker in
                    picker.sourceType = .camera
                    picker.allowsEditing = false
                }
                .flatMap { $0.rx.didFinishPickingMediaWithInfo }
                .take(1)
            }
            .map { info in
                return info[UIImagePickerController.InfoKey.originalImage.rawValue] as? UIImage
            }

        let galleryImage = input.galleryTrigger
            .flatMapLatest { viewController in
                return UIImagePickerController.rx.createWithParent(viewController) { picker in
                    picker.sourceType = .photoLibrary
                    picker.allowsEditing = false
                }
                .flatMap {
                    $0.rx.didFinishPickingMediaWithInfo
                }
                .take(1)
            }
            .map { info in
                return info[UIImagePickerController.InfoKey.originalImage.rawValue] as? UIImage
            }

        let croppedImage = input.cropTrigger
            .flatMapLatest { viewController in
                return UIImagePickerController.rx.createWithParent(viewController) { picker in
                    picker.sourceType = .photoLibrary
                    picker.allowsEditing = true
                }
                .flatMap { $0.rx.didFinishPickingMediaWithInfo }
                .take(1)
            }
            .map { info in
                return info[UIImagePickerController.InfoKey.editedImage.rawValue] as? UIImage
            }
        
        let image = Observable.of(cameraImage,
                                  galleryImage,
                                  croppedImage)
            .merge()
            .compactMap { $0 }
            .asDriver(onErrorJustReturn: UIImage(named: "image"))
        
        self.output = Output(image: image)
    }
}

extension ImagePickerViewModel {
    struct Input {
        let cameraTrigger: PublishRelay<UIViewController?>
        let galleryTrigger: PublishRelay<UIViewController?>
        let cropTrigger: PublishRelay<UIViewController?>
    }
    struct Output {
        let image: Driver<UIImage?>
    }
}
