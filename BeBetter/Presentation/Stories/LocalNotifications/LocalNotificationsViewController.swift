//
//  LocalNotificationsViewController.swift
//  BeBetter
//
//  Created by Kyryl Nevedrov on 07/11/2019.
//  Copyright © 2019 Kyryl Nevedrov. All rights reserved.
//

import UIKit
import UserNotifications

class LocalNotificationsViewController: UIViewController {
    
    // MARK: - Properties
    private let userNotificationCenter = UNUserNotificationCenter.current()
    
    // MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        //set notifications badge to 0
        UIApplication.shared.applicationIconBadgeNumber = 0
        
        self.userNotificationCenter.delegate = self
        self.requestNotificationAuthorization()
    }
    
    // MARK: - Methods
    private func requestNotificationAuthorization() {
        let authOptions = UNAuthorizationOptions(arrayLiteral: .alert, .badge, .sound)
        self.userNotificationCenter.requestAuthorization(options: authOptions) { (success, error) in
            if let error = error {
                print("Error: ", error)
            }
        }
    }

    private func sendNotification() {
        //create notification content
        let notificationContent = UNMutableNotificationContent()
        notificationContent.title = "Hello"
        notificationContent.body = "I see you!"
        notificationContent.badge = NSNumber(value: 1)
        notificationContent.sound = .default
        
        //add actions to content
        let action = UNNotificationAction(identifier: "deleteAction", title: "Delete", options: [.destructive])
        let textAction = UNTextInputNotificationAction(identifier: "textInputAction", title: "Send you opinion", options: [], textInputButtonTitle: "Send", textInputPlaceholder: "Your message")
        let category = UNNotificationCategory(identifier: "category", actions: [action, textAction], intentIdentifiers: [], options: [.customDismissAction])
        notificationContent.categoryIdentifier = "category"
        userNotificationCenter.setNotificationCategories([category])
        
        //add attachment
        if let url = Bundle.main.url(forResource: "image",
                                    withExtension: "jpeg") {
            if let attachment = try? UNNotificationAttachment(identifier: "image",
                                                            url: url,
                                                            options: nil) {
                notificationContent.attachments = [attachment]
            }
        }
        
        //create trigger when notificatin fired (timeInterval, calendar, location)
        let trigger = UNTimeIntervalNotificationTrigger(timeInterval: 5,
                                                        repeats: false)
        //create request for notification with trigger
        let request = UNNotificationRequest(identifier: "appNotification",
                                            content: notificationContent,
                                            trigger: trigger)
        //add request to notificaiton center
        userNotificationCenter.add(request) { (error) in
            if let error = error {
                print("Notification Error: ", error)
            }
        }
    }
    
    // MARK: - @IBOActions
    
    @IBAction func sendNotificationButtonDidTap(_ sender: UIButton) {
        self.sendNotification()
    }
}

extension LocalNotificationsViewController: UNUserNotificationCenterDelegate {
    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
        completionHandler()
    }

    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        completionHandler([.alert, .badge, .sound])
    }
}
