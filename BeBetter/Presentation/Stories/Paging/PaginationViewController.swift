//
//  PaginationViewController.swift
//  BeBetter
//
//  Created by Kyryl Nevedrov on 26/11/2019.
//  Copyright © 2019 Kyryl Nevedrov. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

fileprivate let cellIdentifier = "paginationCell"

class PaginationViewController: UIViewController {
    // MARK: - @IBOutlets
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var searchBar: UISearchBar!
    private let refresher = UIRefreshControl()
    
    // MARK: - Properties
    var viewModel: PaginationViewModel! = PaginationViewModel(useCase: CommentsUseCase())
    private let bag = DisposeBag()
    
    // MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.configureUI()
        self.hideKeyboardWhenTapOutsize()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.configureInputs()
        self.configureOutputs()
    }
    
    // MARK: - Configure UI
    private func configureUI() {
        self.refresher.tintColor = UIColor.green
        self.tableView.refreshControl = refresher
    }
    
    // MARK: - Configure Inputs
    private func configureInputs() {
        let output = self.viewModel.output
        output.finishRefreshing
            .map { !$0 }
            .drive(self.refresher.rx.isRefreshing)
            .disposed(by: bag)
        output.comments.drive(tableView.rx.items(cellIdentifier: cellIdentifier)) {
                row, model, cell in
                cell.textLabel?.text = "\(model.name)"
                cell.detailTextLabel?.text = "\(model.body)"
            }
            .disposed(by: bag)
        output.error
            .drive( onNext: { [weak self] appError in
                self?.show(error: appError)
            })
            .disposed(by: bag)
    }

    // MARK: - Configure Outputs
    private func configureOutputs() {
        let input = self.viewModel.input
        self.tableView.rx.contentOffset
            .throttle(.milliseconds(500), scheduler: MainScheduler.instance)
            .map { [weak self] point in
                guard let self = self else { return false }
                return self.tableView.isNearBottomEdge(edgeOffset: 70)
            }
            .skip(1)
            .filter { $0 }
            .map { _ in return () }
            .bind(to: input.loadNext)
            .disposed(by: bag)
        self.refresher.rx.controlEvent(.valueChanged)
            .bind(to: input.refresh)
            .disposed(by: bag)
        self.searchBar.rx.text.orEmpty
            .throttle(.milliseconds(500), scheduler: MainScheduler.instance)
            .distinctUntilChanged()
            .bind(to: input.searchText)
            .disposed(by: bag)
        input.refresh.accept(())
    }
}
