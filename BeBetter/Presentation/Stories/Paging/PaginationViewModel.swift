//
//  PaginationViewModel.swift
//  BeBetter
//
//  Created by Kyryl Nevedrov on 26/11/2019.
//  Copyright © 2019 Kyryl Nevedrov. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa

final class PaginationViewModel: ViewModel {
    // MARK: - Properties
    var input: Input
    var output: Output
    
    // MARK: - Init
    init(useCase: CommentsUseCase) {
        //Configure Inputs
        self.input = Input(loadNext: PublishRelay(),
                           refresh: PublishRelay(),
                           searchText: BehaviorRelay(value: ""))
        
        var state = State()

        let loadComments = input.loadNext
            .filter { !state.isLoading && !state.isReachedLimit }
            .scan(0) { (offset, _) -> Int in
                return state.isRefreshed ? state.limit : offset + state.limit
            }
            .do(onNext: { _ in state.isLoading = true })
            .map { [input] offset in
                (offset, input.searchText.value)
            }
            .flatMap { useCase.getComments(limit: state.limit, offset: $0.0, searchText: $0.1) }
            .do(onNext: { _ in state.isLoading = false })
            .share()
            
        
        let refreshComments = Observable.of(
                input.refresh.asObservable().map { () },
                input.searchText.asObservable().map { _ in () }
            )
            .merge()
            .filter { !state.isLoading }
            .do(onNext: { _ in
                state.isLoading = true
                state.isRefreshing = true
                state.isReachedLimit = false
            })
            .flatMap { [input] in useCase.getComments(limit: state.limit, offset: 0, searchText: input.searchText.value) }
            .share()
            .do(onNext: { _ in
                state.isLoading = false
                state.isRefreshed = true
            })
            
        let commentsLoadSuccess = Observable.of(
                refreshComments,
                loadComments
            )
            .merge()
            .compactMap { $0.value }
            .do(onNext: { comments in state.isReachedLimit = comments.count < state.limit })
            .scan([]) { (comments: [Comment], newComments) in
                return state.isRefreshing ? newComments : comments + newComments
            }
            .do(onNext: { _ in state.isRefreshing = false })
            .asDriver(onErrorJustReturn: [])
        
        let commetnsLoadFailed = Observable.of(
                refreshComments,
                loadComments
            )
            .merge()
            .compactMap { $0.error }
            .asDriver(onErrorJustReturn: AppError.unknown(message: "unknown error"))
        
        let finishRefreshing = Observable.of(
            commetnsLoadFailed.asObservable().map { _ in true },
            commentsLoadSuccess.asObservable().map { _ in true }
        )
        .merge()
        .asDriver(onErrorJustReturn: true)

        //Configure Outputs
        self.output = Output(comments: commentsLoadSuccess,
                             finishRefreshing: finishRefreshing,
                             error: commetnsLoadFailed)
    }
}

extension PaginationViewModel {
    struct State {
        let limit = 15
        var isLoading = false
        var isRefreshing = false
        var isRefreshed = false
        var isReachedLimit = false
    }
}

extension PaginationViewModel {
    struct Input {
        let loadNext: PublishRelay<Void>
        let refresh: PublishRelay<Void>
        let searchText: BehaviorRelay<String>
    }
    
    struct Output {
        let comments: Driver<[Comment]>
        let finishRefreshing: Driver<Bool>
        let error: Driver<AppError>
    }
}
