//
//  PhotoLibraryViewController.swift
//  BeBetter
//
//  Created by Kyryl Nevedrov on 08/11/2019.
//  Copyright © 2019 Kyryl Nevedrov. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
import Photos

class PhotoLibraryViewController: UIViewController {
    // MARK: - @IBOutlets
    @IBOutlet private weak var photosCollectionView: UICollectionView!
    @IBOutlet private weak var albumsSegmentedControl: UISegmentedControl!
    @IBOutlet private weak var photosCollectionViewFlowLayout: UICollectionViewFlowLayout!
    
    // MARK: - Properties
    private let viewModel: PhotoLibraryViewModel = PhotoLibraryViewModel()
    private let disposeBag = DisposeBag()
    private var cellSize: CGSize {
        let itemsPerRow: CGFloat = 4
        let screenWidth = UIScreen.main.bounds.width
        let cellWidth = (screenWidth - space * (itemsPerRow - 1)) / itemsPerRow
        return CGSize(width: cellWidth, height: cellWidth)
    }
    private let space: CGFloat = 10
    private let imageManager = PHImageManager()
    
    // MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        //PHPhotoLibrary.shared().register(self)
        self.configureUI()
        self.configureVM()
        self.configureRX()
    }
    
    // MARK: - Configure UI
    private func configureUI() {
        //configure flow layout
        self.photosCollectionViewFlowLayout.itemSize = cellSize
        self.photosCollectionViewFlowLayout.minimumLineSpacing = space
        self.photosCollectionViewFlowLayout.minimumInteritemSpacing = space
     
    }
    
    // MARK: - Configure VM
    private func configureVM() {
        self.viewModel.selectedAlbum
            .filter { $0 != nil }
            .map { $0!.assets }
            .bind(to: self.photosCollectionView.rx.items(cellIdentifier: PhotoLibraryCollectionViewCell.reuseIdentifier,
                                                         cellType: PhotoLibraryCollectionViewCell.self))
            { [weak self] row, asset, cell in
                guard let self = self else { return }
                cell.assetIdentifier = asset.localIdentifier
                cell.photoImageView.image = nil
                self.imageManager.requestImage(for: asset, targetSize: CGSize(width: self.cellSize.width, height: self.cellSize.height), contentMode: .aspectFill, options: nil, resultHandler: { image, _ in
                    if cell.assetIdentifier == asset.localIdentifier {
                        cell.photoImageView.image = image
                    }
                })
            }.disposed(by: disposeBag)
        self.viewModel.albums
            .bind { [weak self] albums in
                for (index, album) in albums.enumerated() {
                    self?.albumsSegmentedControl.setTitle(album.name, forSegmentAt: index)
                }
            }
            .disposed(by: disposeBag)
    }
    
    // MARK: - Configure RX
    private func configureRX() {
        self.albumsSegmentedControl.rx.selectedSegmentIndex
            .bind(to: self.viewModel.selectAlbum)
            .disposed(by: disposeBag)
        self.photosCollectionView.rx.modelSelected(PHAsset.self)
            .bind { [weak self] asset in
                //loading original image
                let option = PHImageRequestOptions()
                option.isSynchronous = true
                self?.imageManager.requestImage(for: asset, targetSize: PHImageManagerMaximumSize, contentMode: .default, options: option) { (image, info) in
                    guard let image = image else { return }
                    self?.viewModel.savePhotoToCurrentAlbum.accept(image)
                }
            }
            .disposed(by: disposeBag)
    }
}

