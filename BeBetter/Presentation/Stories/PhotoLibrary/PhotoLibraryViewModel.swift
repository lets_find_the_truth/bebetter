//
//  PhotoLibraryViewModel.swift
//  BeBetter
//
//  Created by Kyryl Nevedrov on 10/11/2019.
//  Copyright © 2019 Kyryl Nevedrov. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
import Photos

final class PhotoLibraryViewModel {
    // MARK: - Inputs
    let selectAlbum = PublishRelay<Int>()
    let savePhotoToCurrentAlbum = PublishRelay<UIImage>()
     
    // MARK: - Outputs
    let selectedAlbum = BehaviorRelay<Album?>(value: nil)
    let albums = BehaviorRelay<[Album]>(value: [])
    
    // MARK: - Properties
    private let disposeBag = DisposeBag()
    private let imageManager = PHImageManager()
     
    // MARK: - Init
    init() {
        self.accessToPhotoLibrary()
        self.selectAlbum
            .bind { [weak self] index in
                guard let self = self, self.albums.value.count > index else {
                    return
                }
                self.selectedAlbum.accept(self.albums.value[index])
            }
            .disposed(by: disposeBag)
        self.savePhotoToCurrentAlbum
            .bind { [weak self] photo in
                self?.savePhotoToCurrentAlbum(image: photo)
            }
            .disposed(by: disposeBag)
        
    }
     
    // MARK: - Methods
    private func accessToPhotoLibrary() {
        let status = PHPhotoLibrary.authorizationStatus()
        switch status {
        case .authorized:
            self.loadAlbums()
        case .restricted, .denied:
            UIApplication.openSettings()
        case .notDetermined:
            PHPhotoLibrary.requestAuthorization { status in
                switch status {
                case .authorized:
                    DispatchQueue.main.async {
                        self.loadAlbums()
                    }
                case .restricted, .denied:
                    DispatchQueue.main.async {
                        UIApplication.openSettings()
                    }
                default: break
                }
            }
            default: break
        }
    }
    
    private func loadAlbums() {
        let fetchOptions = PHFetchOptions()
        fetchOptions.sortDescriptors = [NSSortDescriptor(key: "localizedTitle", ascending: true)]
        let smartAlbums = PHAssetCollection.fetchAssetCollections(with: .album, subtype: .any, options: fetchOptions)
        
        fetchOptions.predicate = NSPredicate(format: "mediaType == %d", PHAssetMediaType.image.rawValue)
        let assetsFetchOptions = PHFetchOptions()
        assetsFetchOptions.sortDescriptors = [NSSortDescriptor(key: "creationDate", ascending: false)]
        assetsFetchOptions.predicate = NSPredicate(format: "mediaType == %d", PHAssetMediaType.image.rawValue)
    
        var albums: [Album] = []
        [smartAlbums].forEach {
            $0.enumerateObjects { collection, index, stop in
                let fetchResult = PHAsset.fetchAssets(in: collection, options: assetsFetchOptions)
                guard fetchResult.count > 0 else { return }
                let assets = fetchResult.objects(at: IndexSet(0..<fetchResult.count))
                let album = Album(name: collection.localizedTitle, assets: assets, collection: collection)
                albums.append(album)
            }
        }
        self.albums.accept(albums)
        self.selectAlbum.accept(0)
    }
    
    private func savePhotoToCurrentAlbum(image: UIImage) {
        PHPhotoLibrary.shared().performChanges({
            let creationRequest = PHAssetChangeRequest.creationRequestForAsset(from: image)
            if let assetCollection = self.selectedAlbum.value?.collection {
                let addAssetRequest = PHAssetCollectionChangeRequest(for: assetCollection)
                addAssetRequest?.addAssets([creationRequest.placeholderForCreatedAsset!] as NSArray)
            }
        }, completionHandler: nil)
    }
}
