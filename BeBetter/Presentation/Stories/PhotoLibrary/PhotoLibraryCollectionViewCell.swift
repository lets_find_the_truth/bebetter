//
//  PhotoLibraryCollectionViewCell.swift
//  BeBetter
//
//  Created by Kyryl Nevedrov on 08/11/2019.
//  Copyright © 2019 Kyryl Nevedrov. All rights reserved.
//

import UIKit

class PhotoLibraryCollectionViewCell: UICollectionViewCell {
    static let reuseIdentifier = "PhotoLibraryCollectionViewCell"
    @IBOutlet weak var photoImageView: UIImageView!
    var assetIdentifier: String!
}
