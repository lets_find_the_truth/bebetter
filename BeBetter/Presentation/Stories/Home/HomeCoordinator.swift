//
//  HomeCoordinator.swift
//  BeBetter
//
//  Created by Kyryl Nevedrov on 22/11/2019.
//  Copyright © 2019 Kyryl Nevedrov. All rights reserved.
//

import UIKit
import RxSwift

class HomeCoordinator: BaseCoordinator<Void> {
    // MARK: - Properties
    private let disposeBag: DisposeBag = DisposeBag()
    private var navigationController: UINavigationController!
    private let name: String
    
    // MARK: - init
    init(name: String, navigationController: UINavigationController) {
        self.navigationController = navigationController
        self.name = name
    }
    
    // MARK: - start method
    override func start(with option: DeepLinkOption?) -> Observable<Void> {
        let viewController = UIStoryboard(storyboardType: .Home).initFromStoryboard(viewController: HomeViewController.self)
        let viewModel = HomeViewModel()
        viewController.viewModel = viewModel
        viewModel.output.signOut
            .bind(to: (UIApplication.shared.delegate as! AppDelegate).appCoordinator.signedOut)
            .disposed(by: disposeBag)
        viewModel.input.name.accept(self.name)
        
        self.navigationController.setViewControllers([viewController], animated: false)
        
        return Observable.never()
    }
}
