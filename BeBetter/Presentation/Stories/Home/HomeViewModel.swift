//
//  HomeViewModel.swift
//  BeBetter
//
//  Created by Kyryl Nevedrov on 22/11/2019.
//  Copyright © 2019 Kyryl Nevedrov. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa

final class HomeViewModel: ViewModel {
    // MARK: - Properties
    var input: Input
    var output: Output
    
    // MARK: - Init
    init() {
        //Configure Inputs
        self.input = Input(signOutTrigger: PublishRelay<Void>(),
                           name: BehaviorRelay<String>(value: "Unknown"))
        //Configure Outputs
        let greetings = input.name
            .map {"HELLO \($0), YOU ARE HOME NOW"}
            .asDriver(onErrorJustReturn: "")
        
        self.output = Output(greetings: greetings,
                             signOut: input.signOutTrigger.asObservable())
    }
}

extension HomeViewModel {
    struct Input {
        let signOutTrigger: PublishRelay<Void>
        let name: BehaviorRelay<String>
    }
    
    struct Output {
        let greetings: Driver<String>
        let signOut: Observable<Void>
    }
}
