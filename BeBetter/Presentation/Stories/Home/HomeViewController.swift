//
//  HomeViewController.swift
//  BeBetter
//
//  Created by Kyryl Nevedrov on 22/11/2019.
//  Copyright © 2019 Kyryl Nevedrov. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

class HomeViewController: UIViewController {
    // MARK: - @IBOutlets
    @IBOutlet weak var welcomeLabel: UILabel!
    @IBOutlet weak var signOutButton: UIButton!
    
    // MARK: - Properties
    var viewModel: HomeViewModel!
    private let bag = DisposeBag()
    
    // MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.configureInputs()
        self.configureOutputs()
    }
    
    // MARK: - Configure Inputs
    private func configureInputs() {
        let output = self.viewModel.output
        output.greetings
            .drive(welcomeLabel.rx.text)
            .disposed(by: bag)
    }

    // MARK: - Configure Outputs
    private func configureOutputs() {
        let input = self.viewModel.input
        signOutButton.rx.tap
            .bind(to: input.signOutTrigger)
            .disposed(by: bag)
    }
}
