//
//  CollectionViewViewController.swift
//  BeBetter
//
//  Created by Kyryl Nevedrov on 25/10/2019.
//  Copyright © 2019 Kyryl Nevedrov. All rights reserved.
//

import UIKit

class CollectionViewViewController: UIViewController {
    
    // MARK: - @IBOutlets
    @IBOutlet weak var horizontalCollectionView: UICollectionView!
    @IBOutlet weak var horizontalCollectionViewLayout: UICollectionViewFlowLayout!
    
    // MARK: - Properties
    var viewModel: CollectionViewViewModel! = CollectionViewViewModel()
    
    // MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.configureUI()
    }
    
    // MARK: - ConfigureUI
    private func configureUI() {
       //auto width
        horizontalCollectionViewLayout.estimatedItemSize = CGSize(width: 100, height: 20)
    }
}

extension CollectionViewViewController: UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {}
}

extension CollectionViewViewController: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.viewModel.collection.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: HorizontalCollectionViewCell.reuseIdentifier, for: indexPath) as! HorizontalCollectionViewCell
        let person = self.viewModel.collection[indexPath.row]
        cell.titleLabel.text = person.name
        return cell
    }
}

extension CollectionViewViewController: UICollectionViewDragDelegate {
    func collectionView(_ collectionView: UICollectionView, itemsForBeginning session: UIDragSession, at indexPath: IndexPath) -> [UIDragItem] {
        let item = self.viewModel.collection[indexPath.row]
        let itemProvider = NSItemProvider(object: item.name as NSString)
        let dragItem = UIDragItem(itemProvider: itemProvider)
        return [dragItem]
    }
}

// MARK: - drag and drop
/// for using set drag and drop delegate to self, and dragInteractionEnabled = true
extension CollectionViewViewController: UICollectionViewDropDelegate {
    func collectionView(_ collectionView: UICollectionView, performDropWith coordinator: UICollectionViewDropCoordinator) {
        var destinationIndexPath: IndexPath
        if let indexPath = coordinator.destinationIndexPath {
            destinationIndexPath = indexPath
        } else {
            let row = collectionView.numberOfItems(inSection: 0)
            destinationIndexPath = IndexPath(item: row, section: 0)
        }
        if coordinator.proposal.operation == .move {
            self.reorderItems(coordinator: coordinator, destinationIndexPath: destinationIndexPath, collectionView: collectionView)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, dropSessionDidUpdate session: UIDropSession, withDestinationIndexPath destinationIndexPath: IndexPath?) -> UICollectionViewDropProposal {
        guard collectionView.hasActiveDrag else {
            return UICollectionViewDropProposal(operation: .forbidden)
        }
        return UICollectionViewDropProposal(operation: .move, intent: .insertAtDestinationIndexPath)
    }
    
    private func reorderItems(coordinator: UICollectionViewDropCoordinator, destinationIndexPath: IndexPath, collectionView: UICollectionView) {
        guard let item = coordinator.items.first, let sourceIndexPath = item.sourceIndexPath else  {
            return
        }
        
        collectionView.performBatchUpdates({ [weak self] in
            guard let self = self else { return }
            let item = self.viewModel.collection[sourceIndexPath.item]
            self.viewModel.collection.remove(at: sourceIndexPath.item)
            self.viewModel.collection.insert(item, at: destinationIndexPath.item)
            collectionView.deleteItems(at: [sourceIndexPath])
            collectionView.insertItems(at: [destinationIndexPath])
        }, completion: nil)
        coordinator.drop(item.dragItem, toItemAt: destinationIndexPath)
    }

}
