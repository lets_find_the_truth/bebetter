//
//  CollectionViewCollectionViewCell.swift
//  BeBetter
//
//  Created by Kyryl Nevedrov on 25/10/2019.
//  Copyright © 2019 Kyryl Nevedrov. All rights reserved.
//

import UIKit

class HorizontalCollectionViewCell: UICollectionViewCell {
    static let reuseIdentifier = "HorizontalCollectionViewCell"
    
    @IBOutlet weak var titleLabel: UILabel!
    
    // auto sizing 
    /*
    override func preferredLayoutAttributesFitting(_ layoutAttributes: UICollectionViewLayoutAttributes) -> UICollectionViewLayoutAttributes {
        layoutIfNeeded()
        
        let size = contentView.systemLayoutSizeFitting(layoutAttributes.size)
        var frame = layoutAttributes.frame
        frame.size.width = ceil(size.width)
        layoutAttributes.frame = frame
        
        return layoutAttributes
    }
    */
}
