//
//  ReactiveCollectionViewViewController.swift
//  BeBetter
//
//  Created by Kyryl Nevedrov on 25/10/2019.
//  Copyright © 2019 Kyryl Nevedrov. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

class ReactiveCollectionViewViewController: UIViewController {
    // MARK: - @IBOutlets
    @IBOutlet weak var carouselCollectionView: UICollectionView!
    @IBOutlet weak var carouselCollectionViewFlowLayout: UICollectionViewFlowLayout!
    
    // MARK: - Properties
    var viewModel: CollectionViewViewModel! = CollectionViewViewModel()
    private let bag = DisposeBag()
    private let spacer: CGFloat = 30
    private var itemSize: CGSize {
        let screenWidth = UIScreen.main.bounds.width
        let screenHeight = UIScreen.main.bounds.height
        let itemWidth = screenWidth - spacer * 2
        let itemHeight = screenHeight  - spacer * 2 - statusAndNavBarHeight
        return CGSize(width: itemWidth, height: itemHeight)
    }
    
    // MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.configureUI()
        self.configureInputs()
        self.configureRx()
    }
    
    // MARK: - Configure UI
    private func configureUI() {
        self.carouselCollectionViewFlowLayout.itemSize = self.itemSize
        self.carouselCollectionViewFlowLayout.minimumLineSpacing = self.spacer
        self.carouselCollectionViewFlowLayout.minimumInteritemSpacing = self.spacer
    }
    
    // MARK: - Configure Inputs
    private func configureInputs() {
        let output = self.viewModel.output
        output.collection
            .bind(to: self.carouselCollectionView.rx.items(cellIdentifier: CarouselCollectionViewCell.reuseIdentifier,
                                                           cellType: CarouselCollectionViewCell.self))
            { row, model, cell in
                cell.titleLabel.text = model.name
            }
            .disposed(by: bag)
    }

    // MARK: - Configure Rx
    private func configureRx() {
        Observable.of(self.carouselCollectionView.rx.didEndDragging.filter { !$0 }.map {_ in ()}.asObservable(),
                      self.carouselCollectionView.rx.didEndDecelerating.asObservable())
            .merge()
            .withLatestFrom(self.carouselCollectionView.rx.contentOffset)
            .bind { [weak self] offset in
                self?.didScroll(offset: offset)
            }
            .disposed(by: bag)
        
        self.carouselCollectionView.rx.didEndScrollingAnimation
            .bind {[weak self] _ in
                self?.endScrollingAnimation()
            }
            .disposed(by: bag)
        self.carouselCollectionView.rx.willBeginDragging
            .bind {[weak self] _ in
                self?.beginDragging()
            }
            .disposed(by: bag)
    }
    
    // MARK: - Methods
    private func didScroll(offset: CGPoint) {
        let itemSizeWithSpacer = self.itemSize.width + self.spacer
        let index = (offset.x + self.carouselCollectionView.contentInset.left) / itemSizeWithSpacer
        let roundedIndex = round(index)
        let indexPath = IndexPath(row: Int(roundedIndex), section: 0)
        self.carouselCollectionView.scrollToItem(at: indexPath, at: .centeredHorizontally, animated: true)
    }
    
    private func endScrollingAnimation() {
        guard let visibleCell = self.carouselCollectionView.visibleCells.first else {
            return
        }
        visibleCell.transform = CGAffineTransform(scaleX: 1.1, y: 1.1)
    }
    
    private func beginDragging() {
        self.carouselCollectionView.visibleCells.forEach { (cell) in
            cell.transform = CGAffineTransform(scaleX: 1, y: 1)
        }
    }
}
