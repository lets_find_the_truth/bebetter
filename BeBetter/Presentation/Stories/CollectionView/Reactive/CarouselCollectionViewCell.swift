//
//  CarouselCollectionViewCell.swift
//  BeBetter
//
//  Created by Kyryl Nevedrov on 03/12/2019.
//  Copyright © 2019 Kyryl Nevedrov. All rights reserved.
//

import UIKit

class CarouselCollectionViewCell: UICollectionViewCell {
    static let reuseIdentifier = "carouselCell"
    
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
}
