//
//  CollectinViewViewModel.swift
//  BeBetter
//
//  Created by Kyryl Nevedrov on 25/10/2019.
//  Copyright © 2019 Kyryl Nevedrov. All rights reserved.
//
import Foundation
import RxCocoa
import RxSwift

final class CollectionViewViewModel: ViewModel {
    // MARK: - Properties
    var input: Input
    var output: Output
    
    // MARK: - Properties
    private let disposeBag = DisposeBag()
    var collection = [
        Person(name: "Tom", followingState: .followed),
        Person(name: "Bill", followingState: .notConnected),
        Person(name: "Smith", followingState: .pending),
        Person(name: "L", followingState: .followed),
        Person(name: "VeryLongNameVeryLongNameVeryLongName", followingState: .notConnected),
        Person(name: "Smith", followingState: .pending),
        Person(name: "Tom", followingState: .followed),
        Person(name: "Bill", followingState: .notConnected),
        Person(name: "Smith", followingState: .pending),
        Person(name: "Tom", followingState: .followed),
        Person(name: "Bill", followingState: .notConnected),
        Person(name: "Smith", followingState: .pending),
        Person(name: "Tom", followingState: .followed),
        Person(name: "Bill", followingState: .notConnected),
        Person(name: "Smith", followingState: .pending),
        Person(name: "Tom", followingState: .followed),
        Person(name: "Bill", followingState: .notConnected),
        Person(name: "Smith", followingState: .pending)
    ]
    
    // MARK: - Init
    init() {
        self.input = Input(load: PublishRelay<Void>())
        self.output = Output(collection: BehaviorRelay<[Person]>(value: collection))
    }
}

extension CollectionViewViewModel {
    struct Input {
        let load: PublishRelay<Void>
    }
    struct Output {
        let collection: BehaviorRelay<[Person]>
    }
}
