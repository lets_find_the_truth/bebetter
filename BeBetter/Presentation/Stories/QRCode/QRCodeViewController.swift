//
//  QRCodeViewController.swift
//  BeBetter
//
//  Created by Kyryl Nevedrov on 07/12/2019.
//  Copyright © 2019 Kyryl Nevedrov. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

class QRCodeViewController: UIViewController {
    // MARK: - @IBOutlets
    @IBOutlet weak var qrCodeView: QRCodeView!
    @IBOutlet weak var codeLabel: UILabel!
    
    // MARK: - Properties
    var viewModel: QRCodeViewModel! = QRCodeViewModel()
    private let bag = DisposeBag()
    
    // MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.configureInputs()
        self.configureOutputs()
    }
    
    // MARK: - Configure Inputs
    private func configureInputs() {
        let output = self.viewModel.output
        output.code
            .drive(codeLabel.rx.text)
            .disposed(by: bag)
        self.qrCodeView.scanFail
            .bind {[weak self] error in self?.show(error: error)}
            .disposed(by: bag)
    }

    // MARK: - Configure Outputs
    private func configureOutputs() {
        let input = self.viewModel.input
        self.qrCodeView.metadataOutput.rx.metadataOutput
            .bind(to: input.metadataOutput)
            .disposed(by: bag)
    }

}
