//
//  QRCodeViewModel.swift
//  BeBetter
//
//  Created by Kyryl Nevedrov on 07/12/2019.
//  Copyright © 2019 Kyryl Nevedrov. All rights reserved.
//

import RxSwift
import RxCocoa
import AVFoundation

final class QRCodeViewModel: ViewModel {
    // MARK: - Properties
    var input: Input
    var output: Output
    
    // MARK: - Init
    init() {
        self.input = Input(metadataOutput: PublishRelay<[AVMetadataObject]>())
        
        let code = input.metadataOutput
            .filter { !$0.isEmpty }
            .compactMap { $0.first! as? AVMetadataMachineReadableCodeObject }
            .compactMap { $0.stringValue }
            .asDriver(onErrorJustReturn: "")
        
        self.output = Output(code: code)
    }
}

extension QRCodeViewModel {
    struct Input {
        let metadataOutput: PublishRelay<[AVMetadataObject]>
    }
    struct Output {
        let code: Driver<String>
    }
}
