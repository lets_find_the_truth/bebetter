//
//  QRCodeView.swift
//  BeBetter
//
//  Created by Kyryl Nevedrov on 07/12/2019.
//  Copyright © 2019 Kyryl Nevedrov. All rights reserved.
//

import UIKit
import AVFoundation
import RxCocoa

class QRCodeView: UIView {
    //MARK: - Properties
    var captureSession: AVCaptureSession?
    let metadataOutput = AVCaptureMetadataOutput()
    var scanFail = PublishRelay<AppError>()
    
    //MARK: - init
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.configureSession()
    }
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.configureSession()
    }
    
    //MARK: - change layer to AVLayer
    override class var layerClass: AnyClass  {
        return AVCaptureVideoPreviewLayer.self
    }
    
    override var layer: AVCaptureVideoPreviewLayer {
        return super.layer as! AVCaptureVideoPreviewLayer
    }
    
    //MARK: - configureSession
    private func configureSession() {
        self.captureSession = AVCaptureSession()
        guard let videoCaptureDevice = AVCaptureDevice.default(for: .video) else { return }
        let videoInput: AVCaptureDeviceInput
        do {
            videoInput = try AVCaptureDeviceInput(device: videoCaptureDevice)
        } catch {
            return
        }
        
        if (captureSession?.canAddInput(videoInput) ?? false) &&
           (captureSession?.canAddOutput(metadataOutput) ?? false) {
            captureSession?.addInput(videoInput)
            captureSession?.addOutput(metadataOutput)
            metadataOutput.metadataObjectTypes = [.qr, .ean8, .ean13, .pdf417]
        } else {
            self.scanFail.accept(AppError.unknown(message: "Scanning not supported"))
            return
        }
        
        self.layer.session = captureSession
        self.layer.videoGravity = .resizeAspectFill
        
        captureSession?.startRunning()
    }

}
