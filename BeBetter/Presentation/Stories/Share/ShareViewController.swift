//
//  ShareViewController.swift
//  BeBetter
//
//  Created by Kyryl Nevedrov on 26/11/2019.
//  Copyright © 2019 Kyryl Nevedrov. All rights reserved.
//

import UIKit

class ShareViewController: UIViewController {
    
    // MARK: - Lifecycle
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.share(text: "I love you", image: UIImage(named: "image")!)
    }
    
    //share text and image
    func share(text: String, image: UIImage) {
        let activityController = UIActivityViewController(activityItems: [text, image], applicationActivities: nil)
        activityController.popoverPresentationController?.sourceView = self.view //for crush in iPad
        activityController.excludedActivityTypes = [.airDrop, .postToFacebook]
        present(activityController, animated: true, completion: nil)
    }

}
