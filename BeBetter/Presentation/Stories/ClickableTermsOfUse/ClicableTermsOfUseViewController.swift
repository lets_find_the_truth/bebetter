//
//  ClicableTermsOfUseViewController.swift
//  BeBetter
//
//  Created by Kyryl Nevedrov on 24/10/2019.
//  Copyright © 2019 Kyryl Nevedrov. All rights reserved.
//

import UIKit

class ClicableTermsOfUseViewController: UIViewController {
    // MARK: - @IBOutlets
    @IBOutlet weak var termsOfUseTextView: UITextView!
    @IBOutlet weak var termsOfUseLabel: UILabel!

    // MARK: - Properties
    private let termsText = "By register, I agree to Terms of Service and Private Policy"
    private let temsOfServiceText = "Terms of Service"
    private let privacyPolicyText = "Private Policy"
    private let termsLinkString = "https://google.com"
    private let privacyLinkString = "https://facebook.com"
    
    // MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.configureUI()
    }
    
    // MARK: - ConfigureUI
    private func configureUI() {
        let textAttributes: [NSAttributedString.Key: Any] = [
            .font: UIFont(.gothamProBol(size: 20))
        ]
        
        //text view with cliable links - BETTER solution
        let attributedText = NSMutableAttributedString(string: self.termsText, attributes: textAttributes)
        attributedText.add(link: self.termsLinkString, to: self.temsOfServiceText)
        attributedText.add(link: self.privacyLinkString, to: self.privacyPolicyText)
        self.termsOfUseTextView.attributedText = attributedText
        self.termsOfUseTextView.linkTextAttributes = [
            .foregroundColor: UIColor.green,
            .underlineStyle: NSUnderlineStyle.single.rawValue
        ]
        self.termsOfUseTextView.delegate = self
        
        //label with cliable links
        let underlineAttriString = NSMutableAttributedString(string: self.termsText, attributes: textAttributes)
        underlineAttriString.addAttribute(.underlineStyle, value: NSUnderlineStyle.single.rawValue, to: self.temsOfServiceText)
        underlineAttriString.addAttribute(.underlineStyle, value: NSUnderlineStyle.single.rawValue, to: self.privacyPolicyText)
        underlineAttriString.addAttribute(.foregroundColor, value: UIColor.green, to: temsOfServiceText)
        underlineAttriString.addAttribute(.foregroundColor, value: UIColor.green, to: privacyPolicyText)
        self.termsOfUseLabel.attributedText = underlineAttriString
        let tap = UITapGestureRecognizer(target: self, action: #selector(privacyLabelDidTap))
        self.termsOfUseLabel.isUserInteractionEnabled = true
        self.termsOfUseLabel.addGestureRecognizer(tap)
    }
    
    // MARK: - Methods
    @objc func privacyLabelDidTap(sender: UITapGestureRecognizer) {
        let termsNSString = NSString(string: self.termsText)
        let termsRange = termsNSString.range(of: self.temsOfServiceText)
        let privacyRange = termsNSString.range(of: self.privacyPolicyText)

        if sender.didTapAttributedTextInLabel(label: termsOfUseLabel, inRange: termsRange) {
            UIApplication.open(url: URL(string: termsLinkString))
        } else if sender.didTapAttributedTextInLabel(label: termsOfUseLabel, inRange: privacyRange) {
            UIApplication.open(url: URL(string: privacyLinkString))
        }
    }
}

extension ClicableTermsOfUseViewController: UITextViewDelegate {
    func textView(_ textView: UITextView, shouldInteractWith URL: URL, in characterRange: NSRange, interaction: UITextItemInteraction) -> Bool {
        return true
    }
}

