//
//  SideMenuTableViewController.swift
//  BeBetter
//
//  Created by Kyryl Nevedrov on 05/12/2019.
//  Copyright © 2019 Kyryl Nevedrov. All rights reserved.
//

import UIKit

class SideMenuTableViewController: UITableViewController {
    
    // MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
    }

    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        //for real app move logic to coordinator
        switch indexPath.row {
        case 2:
            self.show(error: AppError.unknown(message: "Log out"))
        default:
            break
        }
    }
}
