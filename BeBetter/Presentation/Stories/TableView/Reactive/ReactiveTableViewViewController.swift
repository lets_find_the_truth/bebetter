//
//  CustomUIReactiveTableViewViewController.swift
//  BeBetter
//
//  Created by Kyryl Nevedrov on 02/10/2019.
//  Copyright © 2019 Kyryl Nevedrov. All rights reserved.
//

import UIKit
import RxCocoa
import RxSwift

class ReactiveTableViewViewController: UIViewController {
    // MARK: - @IBOutlets
    @IBOutlet weak var tableView: UITableView!
    
    // MARK: - Properties
    private var bag = DisposeBag()
    var viewModel: TableViewViewModel! = TableViewViewModel()
    let action = PublishRelay<Person>()
    
    // MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.configureUI()
        self.configureInputs()
    }
    
    // MARK: - ConfigureUI
    private func configureUI() {
       //auto height
       self.tableView.rowHeight = UITableView.automaticDimension
       self.tableView.estimatedRowHeight = 100
    }
    
    // MARK: - Configure Inputs
    private func configureInputs() {
        let output = self.viewModel.output
        output.persons
            .drive(self.tableView.rx.items(cellIdentifier: ButtonWithStatesTableViewCell.reuseIdentifier, cellType: ButtonWithStatesTableViewCell.self)) { [weak self] row, model, cell in
                guard let self = self else { return }
                cell.configure(with: TableViewCellViewModel(person: model))
                cell.button.rx.tap
                    .map { model }
                    .bind(to: self.viewModel.input.followAction)
                    .disposed(by: cell.disposeBag)
            }
            .disposed(by: bag)
    }
}

