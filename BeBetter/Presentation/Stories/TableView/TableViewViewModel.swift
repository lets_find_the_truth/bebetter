//
//  CustomUITableViewViewModel.swift
//  BeBetter
//
//  Created by Kyryl Nevedrov on 03/10/2019.
//  Copyright © 2019 Kyryl Nevedrov. All rights reserved.
//

import Foundation
import RxCocoa
import RxSwift

final class TableViewViewModel: ViewModel {
    // MARK: - Properties
    var input: Input
    var output: Output
    
    enum PersonCollectionAction {
        case refreshPersons(with: [Person])
        case followAction(_ person: Person)
        case followActionError(_ person: Person)
    }
    
    // MARK: - Test data
    private let personsArray = [
        Person(name: "Tom", followingState: .followed),
        Person(name: "Bill", followingState: .notConnected),
        Person(name: "Smith", followingState: .pending),
        Person(name: "Tom2", followingState: .followed),
        Person(name: "Bill2", followingState: .notConnected),
        Person(name: "Smith2", followingState: .pending),
        Person(name: "Tom3", followingState: .followed),
        Person(name: "Bill3", followingState: .notConnected),
        Person(name: "Smith3", followingState: .pending),
        Person(name: "Tom4", followingState: .followed),
        Person(name: "Bill4", followingState: .notConnected),
        Person(name: "Smith4", followingState: .pending),
        Person(name: "Tom5", followingState: .followed),
        Person(name: "Bill5", followingState: .notConnected),
        Person(name: "Smith5", followingState: .pending),
        Person(name: "Tom6", followingState: .followed),
        Person(name: "Bill6", followingState: .notConnected),
        Person(name: "Smith6", followingState: .pending),
        Person(name: "Tom7", followingState: .followed),
        Person(name: "Bill7", followingState: .notConnected),
        Person(name: "Smith7", followingState: .pending),
        Person(name: "Tom8", followingState: .followed),
        Person(name: "Bill8", followingState: .notConnected),
        Person(name: "Smith8", followingState: .pending),
        Person(name: "Tom9", followingState: .followed),
        Person(name: "Bill9", followingState: .notConnected),
        Person(name: "Smith9", followingState: .pending)
    ]
    
    // MARK: - Init
    init() {
        self.input = Input(followAction: PublishRelay<Person>())
        
        let collection = Observable.of(personsArray)
            .map { persons -> PersonCollectionAction in .refreshPersons(with: persons)}
        let followAction = input.followAction
            .map { person -> PersonCollectionAction in .followAction(person)}
        
        // MARK: - use case functions for testing only
        func changeFollowState(person: Person) -> Single<RequestResult<Void, AppError>> {
            return Single.create { single in
                single(.success(RequestResult(value: nil, error: AppError.unknown(message: "Can't login"))))
                return Disposables.create()
            }
        }
        
        let followActionError = input.followAction
            .flatMapLatest({ (person) -> Single<Person?> in
                return changeFollowState(person: person).map { result in
                    guard result.error == nil else {
                        return person
                    }
                    return nil
                }
            })
            .compactMap { $0 }
            .map { person -> PersonCollectionAction in .followActionError(person)}
        
        let items = Observable.of(followAction,
                                  collection,
                                  followActionError)
            .merge()
            .scan([], accumulator: { (currentPersons, action) -> [Person] in
                switch action {
                case .followAction(let person):
                    let persons = currentPersons
                    persons.filter { $0.name == person.name }.first?.followingState = .followed
                    return persons
                case .followActionError(let person):
                    let persons = currentPersons
                    persons.filter { $0.name == person.name }.first?.followingState = person.followingState
                    return persons
                case .refreshPersons(with: let persons):
                    return persons
                }
            })
            .asDriver(onErrorJustReturn: [])
        
        self.output = Output(persons: items)
    }
}

extension TableViewViewModel {
    struct Input {
        let followAction: PublishRelay<Person>
    }
    
    struct Output {
        let persons: Driver<[Person]>
    }
}
