//
//  TableViewCellViewModel.swift
//  BeBetter
//
//  Created by Kyryl Nevedrov on 03/10/2019.
//  Copyright © 2019 Kyryl Nevedrov. All rights reserved.
//

import RxCocoa
import RxSwift

final class TableViewCellViewModel: CellViewModel {
    // MARK: - Properties
    var input: Input
    var output: Output
    
    // MARK: - Init
    init(person: Person) {
        self.input = Input(followButtonAction: PublishRelay<ButtonWithStates.ButtonState>())
        let name = Observable.of(person.name)
            .asDriver(onErrorJustReturn: "")
        let buttonState = ButtonWithStates.ButtonState.init(rawValue: person.followingState.rawValue) ?? .followed
        let stateObservable = Observable
            .of(buttonState)
        let followAction = input.followButtonAction
            .map { state -> ButtonWithStates.ButtonState in
                switch state {
                case .followed:
                    return .notConnected
                case .notConnected:
                    return .pending
                case .pending:
                    return .followed
                }
            }
            .asObservable()
        
        let followingState = Observable.of(stateObservable,
                                           followAction)
            .merge()
            .asDriver(onErrorJustReturn: .followed)
        
        self.output = Output(name: name,
                             followingState: followingState)
    }
}

extension TableViewCellViewModel {
    struct Input {
        let followButtonAction: PublishRelay<ButtonWithStates.ButtonState>
    }
    struct Output {
        let name: Driver<String>
        let followingState: Driver<ButtonWithStates.ButtonState>
    }
    
}
