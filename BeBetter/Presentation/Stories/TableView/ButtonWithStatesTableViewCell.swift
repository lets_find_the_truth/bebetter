//
//  CustomUIButtonWithStatesTableViewCell.swift
//  BeBetter
//
//  Created by Kyryl Nevedrov on 02/10/2019.
//  Copyright © 2019 Kyryl Nevedrov. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

class ButtonWithStatesTableViewCell: UITableViewCell {
    static let reuseIdentifier = "CustomUIButtonWithStatesTableViewCell"
    
    // MARK: - @IBOutlets
    @IBOutlet weak var button: ButtonWithStates!
    @IBOutlet weak var nameLabel: UILabel!
    
    // MARK: - Properties
    var disposeBag = DisposeBag()
    private var viewModel: TableViewCellViewModel!
    
    // MARK: - Methods
    func configure(with viewModel: TableViewCellViewModel) {
        self.viewModel = viewModel
        viewModel.output.name
            .drive(self.nameLabel.rx.text)
            .disposed(by: disposeBag)
        viewModel.output.followingState
            .drive(onNext: { [weak self] state in
                self?.button.buttonState = state
            })
            .disposed(by: disposeBag)
        self.button.rx.tap
            .map { [weak self] _ in self?.button.buttonState ?? .followed}
            .bind(to: viewModel.input.followButtonAction)
            .disposed(by: disposeBag)
    }
    
    override func prepareForReuse() {
        self.disposeBag = DisposeBag()
    }
}
