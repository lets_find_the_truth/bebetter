//
//  LocalizationViewController.swift
//  BeBetter
//
//  Created by Kyryl Nevedrov on 23/10/2019.
//  Copyright © 2019 Kyryl Nevedrov. All rights reserved.
//

import UIKit

class LocalizationViewController: UIViewController {
    // MARK: - @IBOutlets
    @IBOutlet weak var codeLabel: UILabel!
    
    // MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        //tips
        //open settings to change language
        //get available languages from server
        //restore state when app relaunces
        
        // to l10z 1) add language to project setting
        // 2) create new Strings file named Localizable
        // 3) if you want plural add StringDict
        // 4) see example of usage
        // 5) from terminal to generate strings genstrings -o en.lproj *.swift
        // 6) you can localize images from assets
        
        //to l10z UI find ObjId and paste into storyboard localization
        //or just add all UI to storyboard and then check localizaion checkbox(replace file if needed) it will create UI elements localization for you
        
        //to l10z app title and copyright add InfoPlist.strings and add to it
        //"CFBundleDisplayName" = "MyApp";
        //"NSHumanReadableCopyright" = "2016 Goldrush Computing Inc. All rights reserved.";
        
        //you can also export/import your localization files view editor->export/import l10n
        //you can use internalizaion tool like xclocReaded
        
        //Localize label text from code, for plural
        self.codeLabel.text = String(format: Localization.Global.youWereHere.string, 10)
        
        let serverLanguages = ["en-EN"] //get from server
        //get preferred language
        let _ = Bundle.preferredLocalizations(from: serverLanguages).first
    }
    

    @IBAction func showSettingButtonDidTap(_ sender: UIButton) {
        //launch to setting to change language
        UIApplication.openSettings()
    }
}
