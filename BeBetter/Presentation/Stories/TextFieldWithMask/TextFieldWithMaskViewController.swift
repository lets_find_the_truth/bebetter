//
//  TextFieldWithMaskViewController.swift
//  BeBetter
//
//  Created by Kyryl Nevedrov on 06/12/2019.
//  Copyright © 2019 Kyryl Nevedrov. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

class TextFieldWithMaskViewController: UIViewController {
    // MARK: - @IBOutlets
    @IBOutlet weak var textField: UITextField!
    
    // MARK: - Properties
    var viewModel: TextFieldWithMaskViewModel! = TextFieldWithMaskViewModel(mask: "+X (XXX) XXX XX-XX")
    private let bag = DisposeBag()

    // MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.configureInputs()
        self.configureOutputs()
    }

    // MARK: - Configure Inputs
    private func configureInputs() {
        let output = self.viewModel.output
        output.formattedText
            .drive(textField.rx.text)
            .disposed(by: bag)
    }

    // MARK: - Configure Outputs
    private func configureOutputs() {
        let input = self.viewModel.input
        self.textField.rx.text.orEmpty
            .bind(to: input.text)
            .disposed(by: bag)
    }
}
