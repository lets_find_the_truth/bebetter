//
//  TextFieldWithMaskViewModel.swift
//  BeBetter
//
//  Created by Kyryl Nevedrov on 06/12/2019.
//  Copyright © 2019 Kyryl Nevedrov. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa

final class TextFieldWithMaskViewModel: ViewModel {
    // MARK: - Properties
    var input: Input
    var output: Output
    
    // MARK: - Init
    init(mask: String) {
        self.input = Input(text: PublishRelay<String>())
        let formattedText = input.text
            .map {
                TextFieldWithMaskViewModel.formattedNumber(number: $0, mask: mask)
        }.asDriver(onErrorJustReturn: "")
        
        self.output = Output(formattedText: formattedText)
    }
    
    //put inside use case in real app
    private static func formattedNumber(number: String, mask: String) -> String {
        let cleanPhoneNumber = number.components(separatedBy: CharacterSet.decimalDigits.inverted).joined()
        
        var result = ""
        var index = cleanPhoneNumber.startIndex
        for ch in mask {
            if index == cleanPhoneNumber.endIndex {
                break
            }
            if ch == "X" {
                result.append(cleanPhoneNumber[index])
                index = cleanPhoneNumber.index(after: index)
            } else {
                result.append(ch)
            }
        }
        return result
    }
}

extension TextFieldWithMaskViewModel {
    struct Input {
        let text: PublishRelay<String>
    }
    struct Output {
        let formattedText: Driver<String>
    }
}
