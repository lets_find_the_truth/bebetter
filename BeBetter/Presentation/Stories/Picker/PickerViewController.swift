//
//  PickerViewController.swift
//  BeBetter
//
//  Created by Kyryl Nevedrov on 19/11/2019.
//  Copyright © 2019 Kyryl Nevedrov. All rights reserved.
//

import UIKit
import RxCocoa
import RxSwift

class PickerViewController: UIViewController {
    // MARK: - @IBOutlets
    @IBOutlet weak var pickerView: UIPickerView!
    @IBOutlet weak var cancelButton: UIButton!
    @IBOutlet weak var doneButton: UIButton!
    
    // MARK: - Properties
    var viewModel: PickerViewModel!
    private let bag = DisposeBag()
    
    // MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.configureInputs()
        self.configureOutputs()
    }

    // MARK: - Configure Inputs
    private func configureInputs() {
        let output = self.viewModel.output
        output.ages
            .drive(pickerView.rx.itemTitles) { row, item in
                return "\(item)"
            }
            .disposed(by: bag)
        output.initialAgeIndex
            .drive(onNext: { [weak self](index) in
                self?.pickerView.selectRow(index, inComponent: 0, animated: true)
            })
            .disposed(by: bag)
    }

    // MARK: - Configure Outputs
    private func configureOutputs() {
        let input = self.viewModel.input
        self.doneButton.rx.tap
            .bind(to: input.doneTrigger)
            .disposed(by: bag)
        self.cancelButton.rx.tap
            .bind(to: input.canelTrigger)
            .disposed(by: bag)
        self.pickerView.rx.modelSelected(Int.self)
            .compactMap { $0.first }
            .bind(to: input.selectedAge)
            .disposed(by: bag)
    }
}
