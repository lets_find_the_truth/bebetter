//
//  PickerViewModel.swift
//  BeBetter
//
//  Created by Kyryl Nevedrov on 19/11/2019.
//  Copyright © 2019 Kyryl Nevedrov. All rights reserved.
//

import Foundation
import RxCocoa
import RxSwift

final class PickerViewModel: ViewModel {
    // MARK: - Properties
    var input: Input
    var output: Output
    
    // MARK: - Init
    init() {
        //Configure Inputs
        self.input = Input(selectedAge: BehaviorRelay<Int>(value: 0),
                           canelTrigger: PublishRelay<Void>(),
                           doneTrigger: PublishRelay<Void>(),
                           ages: BehaviorRelay<[Int]>(value: []))
        //Configure Outputs
        let ageSelected = input.doneTrigger
            .withLatestFrom(input.selectedAge)
        let outputAges = input.ages
            .asDriver(onErrorJustReturn: [])
        
        let initialAgeIndex = Observable
            .combineLatest(
                input.selectedAge,
                input.ages
            )
            .map { $1.firstIndex(of: $0) ?? 0 }
            .asDriver(onErrorJustReturn: 0)
        
        self.output = Output(didCancel: input.canelTrigger.asObservable(),
                             initialAgeIndex: initialAgeIndex,
                             ageSelected: ageSelected,
                             selectedAge: input.selectedAge.asDriver(onErrorJustReturn: 0),
                             ages: outputAges)
    }
}

// MARK: - Input/Output extension
extension PickerViewModel {
    struct Input {
        let selectedAge: BehaviorRelay<Int>
        let canelTrigger: PublishRelay<Void>
        let doneTrigger: PublishRelay<Void>
        let ages: BehaviorRelay<[Int]>
    }
    
    struct Output {
        let didCancel: Observable<Void>
        let initialAgeIndex: Driver<Int>
        let ageSelected: Observable<Int>
        let selectedAge: Driver<Int>
        let ages: Driver<[Int]>
    }
    
}
