//
//  PickerCoordinator.swift
//  BeBetter
//
//  Created by Kyryl Nevedrov on 19/11/2019.
//  Copyright © 2019 Kyryl Nevedrov. All rights reserved.
//

import UIKit
import RxSwift

enum PickerCoordinationResult {
    case age(Int)
    case cancel
}

class PickerCoordinator: BaseCoordinator<PickerCoordinationResult> {
    // MARK: - Properties
    private let rootViewController: UIViewController
    private let disposeBag: DisposeBag = DisposeBag()
    private let ages: [Int]
    private let age: Int
    
    // MARK: - init
    init(rootViewController: UIViewController, age: Int, ages: [Int]) {
        self.rootViewController = rootViewController
        self.age = age
        self.ages = ages
    }
    
    // MARK: - start method
    override func start(with option: DeepLinkOption?) -> Observable<PickerCoordinationResult> {
        let viewController = UIStoryboard(storyboardType: .Picker).initFromStoryboard(viewController: PickerViewController.self)
        let navigationController = UINavigationController(rootViewController: viewController)
        
        let viewModel = PickerViewModel()
        viewController.viewModel = viewModel
        
        viewModel.input.ages.accept(self.ages)
        viewModel.input.selectedAge.accept(self.age)
    
        let cancel = viewModel.output.didCancel.map { _ in PickerCoordinationResult.cancel }
        let age = viewModel.output.ageSelected.map { PickerCoordinationResult.age($0) }
        
        rootViewController.present(navigationController, animated: true)
        
        return Observable.merge(cancel, age)
            .take(1)
            .do(onNext: { [weak self] _ in self?.rootViewController.dismiss(animated: true) })
    }
}
