//
//  KeyboardViewController.swift
//  BeBetter
//
//  Created by Kyryl Nevedrov on 26/10/2019.
//  Copyright © 2019 Kyryl Nevedrov. All rights reserved.
//

import RxKeyboard
import RxSwift

class KeyboardViewController: UIViewController {
    @IBOutlet weak var heightLabel: UILabel!
    @IBOutlet weak var scrollView: UIScrollView!
    
    // MARK: - Properties
    private let disposeBag = DisposeBag()
    
    // MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.hideKeyboardWhenTapOutsize()
        self.nativeWay()
        //self.rxWay()
    }
    
    // MARK: - Methods
    private func nativeWay() {
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(keyboardFrameChanged),
            name: UIResponder.keyboardWillShowNotification,
            object: nil
        )
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(keyboardFrameChanged),
            name: UIResponder.keyboardWillHideNotification,
            object: nil
        )
    }
    
    @objc func keyboardFrameChanged(notification: Notification) {
        var keyboardHeight: CGFloat = 0
        if let keyboardFrame = notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue,
            notification.name != UIResponder.keyboardWillHideNotification {
            keyboardHeight = keyboardFrame.cgRectValue.height
        }
        self.heightLabel.text = "Keyboard height: \(keyboardHeight)"
        self.scrollView.contentInset.bottom = keyboardHeight
    }
    
    private func rxWay() {
        RxKeyboard.instance.visibleHeight
            .drive(onNext: { [scrollView, heightLabel] keyboardHeight in
                heightLabel?.text = "Keyboard height: \(keyboardHeight)"
                scrollView?.contentInset.bottom = keyboardHeight
            })
            .disposed(by: disposeBag)
    }
}
