//
//  LoginViewModel.swift
//  BeBetter
//
//  Created by Kyryl Nevedrov on 14/11/2019.
//  Copyright © 2019 Kyryl Nevedrov. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa

final class LoginViewModel: ViewModel {
    // MARK: - Properties
    private let useCase: LoginUseCase
    private let validationUseCase: ValidationUseCase
    var input: Input
    var output: Output
    
    // MARK: - Init
    init(useCase: LoginUseCase, validationUseCase: ValidationUseCase) {
        self.useCase = useCase
        self.validationUseCase = validationUseCase
        //Configure Inputs
        self.input = Input(name: PublishRelay<String>(),
                           pass: PublishRelay<String>(),
                           loginTrigger: PublishRelay<Void>(),
                           registrationTrigger: PublishRelay<Void>())
        //Configure Outputs
        let login = input.loginTrigger
            .withLatestFrom(
                Observable.combineLatest(
                    input.name,
                    input.pass
                )
            )
            .flatMapLatest(useCase.login)
        
        let loginSuccess = login
            .compactMap { $0.value }
            .withLatestFrom(input.name)
            .asObservable()
        
        let loginError = login
            .compactMap { $0.error?.localizedDescription }
            .asDriver(onErrorJustReturn: nil)
        
        let isErrorHidden = login
            .map { $0.error == nil}
            .asDriver(onErrorJustReturn: true)
            .startWith(true)
        
        let isLoginButtonEnabled = Observable
            .combineLatest(
                input.name.map(validationUseCase.validateName),
                input.pass.map(validationUseCase.validatePass)
            )
            .map { $0 && $1 }
            .asDriver(onErrorJustReturn: false)
        
        self.output = Output(
            login: loginSuccess,
            registration: input.registrationTrigger.asObservable(),
            isLoginButtonEnabled: isLoginButtonEnabled,
            isErrorHidden: isErrorHidden,
            errorText: loginError
        )
    }
}

// MARK: - Input/Output extension
extension LoginViewModel {
    struct Input {
        let name: PublishRelay<String>
        let pass: PublishRelay<String>
        let loginTrigger: PublishRelay<Void>
        let registrationTrigger: PublishRelay<Void>
    }
    
    struct Output {
        let login: Observable<String>
        let registration: Observable<Void>
        let isLoginButtonEnabled: Driver<Bool>
        let isErrorHidden: Driver<Bool>
        let errorText: Driver<String?>
    }
}
