//
//  RegistrationViewController.swift
//  BeBetter
//
//  Created by Kyryl Nevedrov on 14/11/2019.
//  Copyright © 2019 Kyryl Nevedrov. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

class RegistrationViewController: UIViewController {
    // MARK: - @IBOutlets
    @IBOutlet weak var nameTextField: UITextField!
    @IBOutlet weak var passTextField: UITextField!
    @IBOutlet weak var selectAgeButton: UIButton!
    @IBOutlet weak var ageLabel: UILabel!
    @IBOutlet weak var errorLabel: UILabel!
    @IBOutlet weak var registrationButton: UIButton!
    
    // MARK: - Properties
    private var bag = DisposeBag()
    var viewModel: RegistrationViewModel!
    
    // MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.configureInputs()
        self.configureOutputs()
    }

    // MARK: - Configure Inputs
    private func configureInputs() {
        let output = self.viewModel.output
        output.errorText
            .drive(errorLabel.rx.text)
            .disposed(by: bag)
        output.isRegistrationButtonEnabled
            .drive(registrationButton.rx.isEnabled)
            .disposed(by: bag)
        output.isErrorHidden
            .drive(errorLabel.rx.isHidden)
            .disposed(by: bag)
        output.age
            .drive(ageLabel.rx.text)
            .disposed(by: bag)
    }
    
    // MARK: - Configure Outputs
    private func configureOutputs() {
        let input = self.viewModel.input
        self.nameTextField.rx.text.orEmpty
            .bind(to: input.name)
            .disposed(by: bag)
        self.passTextField.rx.text.orEmpty
            .bind(to: input.pass)
            .disposed(by: bag)
        self.selectAgeButton.rx.tap
            .bind(to: input.ageSelectTrigger)
            .disposed(by: bag)
        self.registrationButton.rx.tap
            .bind(to: input.registrationTrigger)
            .disposed(by: bag)
    }

}
