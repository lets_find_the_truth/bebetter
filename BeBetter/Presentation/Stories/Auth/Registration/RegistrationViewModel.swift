//
//  RegistrationViewModel.swift
//  BeBetter
//
//  Created by Kyryl Nevedrov on 14/11/2019.
//  Copyright © 2019 Kyryl Nevedrov. All rights reserved.
//

import Foundation
import RxCocoa
import RxSwift

final class RegistrationViewModel: ViewModel{
    // MARK: - Properties
    private let useCase: RegistrationUseCase
    private let validationUseCase: ValidationUseCase
    var input: Input
    var output: Output
    
    // MARK: - Init
    init(useCase: RegistrationUseCase, validationUseCase: ValidationUseCase) {
        self.useCase = useCase
        self.validationUseCase = validationUseCase
        
        //Configure Inputs
        self.input = Input(registrationTrigger: PublishRelay<Void>(),
                           ageSelectTrigger: PublishRelay<Void>(),
                           name: PublishRelay<String>(),
                           pass: PublishRelay<String>(),
                           age: BehaviorRelay(value: 20))
        
        //Configure Outputs
        let registration = input.registrationTrigger
            .withLatestFrom(
                Observable.combineLatest(
                    input.name,
                    input.age,
                    input.pass
                )
            )
            .flatMapLatest(useCase.registration)
        
        let registrationSuccess = registration
            .compactMap { $0.value }
            .withLatestFrom(input.name)
        
        let ageSelect = input.ageSelectTrigger
            .withLatestFrom(input.age.asObservable())
        
        let isRegistrationButtonEnabled = Observable
            .combineLatest(
                input.name.map(validationUseCase.validateName),
                input.age.map(validationUseCase.validateAge),
                input.pass.map(validationUseCase.validatePass)
            ).map { $0 && $1 && $2 }
            .asDriver(onErrorJustReturn: false)
        
        let isErrorHidden = registration
            .map { $0.error == nil }
            .asDriver(onErrorJustReturn: true)
            .startWith(true)
        
        let registrationError = registration
            .compactMap { $0.error?.localizedDescription }
            .asDriver(onErrorJustReturn: nil)
        
        let ageText = input.age
            .map { "Age: \($0)" }
            .asDriver(onErrorJustReturn: "")
        
        let ages = Observable.of(Array(1...100))
        
        self.output = Output(registration: registrationSuccess,
                             ageSelect: ageSelect,
                             isRegistrationButtonEnabled: isRegistrationButtonEnabled,
                             isErrorHidden: isErrorHidden,
                             errorText: registrationError,
                             age: ageText,
                             ages: ages)
    }
}

// MARK: - Input/Output extension
extension RegistrationViewModel {
    struct Input {
        let registrationTrigger: PublishRelay<Void>
        let ageSelectTrigger: PublishRelay<Void>
        let name: PublishRelay<String>
        let pass: PublishRelay<String>
        let age: BehaviorRelay<Int>
    }
    
    struct Output {
        let registration: Observable<String>
        let ageSelect: Observable<Int>
        let isRegistrationButtonEnabled: Driver<Bool>
        let isErrorHidden: Driver<Bool>
        let errorText: Driver<String?>
        let age: Driver<String>
        let ages: Observable<[Int]>
    }
}
