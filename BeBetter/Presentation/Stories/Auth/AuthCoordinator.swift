//
//  AuthCoordinator.swift
//  BeBetter
//
//  Created by Kyryl Nevedrov on 22/11/2019.
//  Copyright © 2019 Kyryl Nevedrov. All rights reserved.
//

import UIKit
import RxSwift

class AuthCoordinator: BaseCoordinator<Void> {
    // MARK: - Properties
    private let disposeBag: DisposeBag = DisposeBag()
    private var navigationController: UINavigationController!
    
    // MARK: - init
    init(navigationController: UINavigationController) {
        self.navigationController = navigationController
    }
    
    // MARK: - start method
    override func start(with option: DeepLinkOption?) -> Observable<Void> {
        self.showLogin()
        return Observable.empty()
    }
    
    // MARK: - Methods
    private func showLogin(){
        let viewController = UIStoryboard(storyboardType: .Login).initFromStoryboard(viewController: LoginViewController.self)
        let viewModel = LoginViewModel(useCase: LoginUseCase(), validationUseCase: ValidationUseCase())
        viewController.viewModel = viewModel
        viewModel.output.registration.subscribe({ [weak self] _ in
            self?.showRegistration()
        }).disposed(by: disposeBag)
        viewModel.output.login
            .bind(to: (UIApplication.shared.delegate as! AppDelegate).appCoordinator.signedIn)
            .disposed(by: disposeBag)
        navigationController.setViewControllers([viewController], animated: false)
    }
    
    private func showRegistration() {
        let viewController = UIStoryboard(storyboardType: .Registration).initFromStoryboard(viewController: RegistrationViewController.self)
        let viewModel = RegistrationViewModel(useCase: RegistrationUseCase(), validationUseCase: ValidationUseCase())
        viewController.viewModel = viewModel
        Observable
            .combineLatest(
                viewModel.output.ageSelect,
                viewModel.output.ages
            )
            .flatMap { [weak self] (age, ages) -> Observable<Int?> in
                guard let self = self else { return .empty() }
                return self.showAgePicker(rootViewController: self.navigationController, age: age, ages: ages)
            }
            .compactMap { $0 }
            .bind(to: viewModel.input.age)
            .disposed(by: disposeBag)
        viewModel.output.registration
            .bind(to: (UIApplication.shared.delegate as! AppDelegate).appCoordinator.signedIn)
            .disposed(by: disposeBag)
        self.navigationController.pushViewController(viewController, animated: true)
    }
    
    private func showAgePicker(rootViewController: UINavigationController, age: Int, ages: [Int]) -> Observable<Int?> {
        let pickerCoordinator = PickerCoordinator(rootViewController: rootViewController, age: age, ages: ages)
        return coordinate(to: pickerCoordinator)
            .map { result in
                switch result {
                case .age(let age):
                    return age
                case .cancel:
                    return nil
                }
        }
    }
}
