//
//  DocumentsViewModel.swift
//  BeBetter
//
//  Created by Kyryl Nevedrov on 06/12/2019.
//  Copyright © 2019 Kyryl Nevedrov. All rights reserved.
//

import UIKit.UIImage
import RxSwift
import RxCocoa

final class DocumentsViewModel: ViewModel {
    // MARK: - Properties
    var input: Input
    var output: Output
    
    // MARK: - Init
    init() {
        self.input = Input(imageUrls: PublishRelay<[URL]>())
        
        let image = input.imageUrls
            .filter { !$0.isEmpty }
            .compactMap { UIImage(contentsOfFile: $0.first!.path) }
            .asDriver(onErrorJustReturn: UIImage())
        
        self.output = Output(image: image)
    }
}

extension DocumentsViewModel {
    struct Input {
        let imageUrls: PublishRelay<[URL]>
    }
    struct Output {
        let image: Driver<UIImage>
    }
}
