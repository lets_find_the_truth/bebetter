//
//  DocumentsViewController.swift
//  BeBetter
//
//  Created by Kyryl Nevedrov on 06/12/2019.
//  Copyright © 2019 Kyryl Nevedrov. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
import MobileCoreServices

class DocumentsViewController: UIViewController {
    // MARK: - @IBOutlets
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var openDocumentsButton: UIButton!
    
    // MARK: - Properties
    var viewModel: DocumentsViewModel! = DocumentsViewModel()
    private lazy var picker = UIDocumentPickerViewController(documentTypes: [kUTTypeImage as String], in: .import)
    private let bag = DisposeBag()

    // MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.configureInputs()
        self.configureOutputs()
    }

    // MARK: - Configure Inputs
    private func configureInputs() {
        let output = self.viewModel.output
        output.image
            .drive(imageView.rx.image)
            .disposed(by: bag)
    }

    // MARK: - Configure Outputs
    private func configureOutputs() {
        let input = self.viewModel.input
        self.openDocumentsButton.rx.tap
            .bind { [weak self] in
                guard let self = self else { return }
                self.present(self.picker, animated: true, completion: nil)
            }
            .disposed(by: bag)
        self.picker.rx.didPickDocumentAt
            .bind(to: input.imageUrls)
            .disposed(by: bag)
    }
}
