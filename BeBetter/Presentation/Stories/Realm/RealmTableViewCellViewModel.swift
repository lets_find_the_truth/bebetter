//
//  RealmTableViewCellModel.swift
//  BeBetter
//
//  Created by Kyryl Nevedrov on 29/11/2019.
//  Copyright © 2019 Kyryl Nevedrov. All rights reserved.
//

import RxSwift
import RxCocoa
import RxRealm

final class RealmTableViewCellViewModel: ViewModel {
    // MARK: - Properties
    var input: Input
    var output: Output
    private let bag = DisposeBag()
    
    // MARK: - Init
    init(useCase: BookUseCase, book: Book) {
        //Configure Inputs
        self.input = Input(likeSwitchValue: PublishRelay<Bool>())
        
        let isLiked = Observable.of(book)
            .map { $0.isLiked }
            .asDriver(onErrorJustReturn: false)
        
        let title = Observable.of(book)
            .map { $0.title }
            .asDriver(onErrorJustReturn: "")
        
        self.input.likeSwitchValue
            .skip(1)
            .bind { value in
                value ? useCase.like(book) : useCase.unlike(book)
            }.disposed(by: bag)
      
        //Configure Outputs
        self.output = Output(isLiked: isLiked,
                             title: title)
    }
}

extension RealmTableViewCellViewModel {
    struct Input {
        let likeSwitchValue: PublishRelay<Bool>
    }
    
    struct Output {
        let isLiked: Driver<Bool>
        let title: Driver<String>
    }
}
