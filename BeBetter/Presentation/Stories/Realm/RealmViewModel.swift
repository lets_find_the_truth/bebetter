//
//  RealmViewModel.swift
//  BeBetter
//
//  Created by Kyryl Nevedrov on 29/11/2019.
//  Copyright © 2019 Kyryl Nevedrov. All rights reserved.
//

import RxSwift
import RxCocoa
import RxRealm
import RealmSwift

final class RealmViewModel: ViewModel {
    // MARK: - Properties
    var input: Input
    var output: Output
    let useCase: BookUseCase
    private var bag = DisposeBag()
    
    // MARK: - Init
    init(useCase: BookUseCase) {
        self.useCase = useCase
        //Configure Inputs
        self.input = Input(deleteBook: PublishRelay<Book>(),
                           loadBooks: PublishRelay<Void>(),
                           addBook: PublishRelay<Void>())
        
        let books: BehaviorRelay<[Book]> =  BehaviorRelay<[Book]>(value: [])
        
        let changeSet = self.input.loadBooks
            .flatMapLatest(useCase.getBooks)
            .map ({ (booksArray, set) -> RealmChangeset? in
                books.accept(booksArray)
                return set
            })
            .asDriver(onErrorJustReturn: nil)
        
        input.addBook
            .bind(onNext: { _ in useCase.createRandomBook() })
            .disposed(by: bag)
        
        input.deleteBook
            .bind(onNext: { book in useCase.delete(book) })
            .disposed(by: bag)
      
        //Configure Outputs
        self.output = Output(books: books,
                             booksChangeSet: changeSet)
    }
}

extension RealmViewModel {
    struct Input {
        let deleteBook: PublishRelay<Book>
        let loadBooks: PublishRelay<Void>
        let addBook: PublishRelay<Void>
    }
    
    struct Output {
        let books: BehaviorRelay<[Book]>
        let booksChangeSet: Driver<RealmChangeset?>
    }
}
