//
//  RealmTableViewCell.swift
//  BeBetter
//
//  Created by Kyryl Nevedrov on 29/11/2019.
//  Copyright © 2019 Kyryl Nevedrov. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

class RealmTableViewCell: UITableViewCell {
    static let reuseIdentifier = "realmCell"
    // MARK: - @IBOutlets
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var favoriteSwitch: UISwitch!
    
    // MARK: - Properties
    private var bag = DisposeBag()
    var viewModel: RealmTableViewCellViewModel!
    
    // MARK: - configure
    func configureWith(viewModel: RealmTableViewCellViewModel) {
        self.viewModel = viewModel
        self.viewModel.output.title
            .drive(titleLabel.rx.text)
            .disposed(by: bag)
        self.viewModel.output.isLiked
            .drive(favoriteSwitch.rx.value)
            .disposed(by: bag)
        self.favoriteSwitch.rx.value
            .bind(to: viewModel.input.likeSwitchValue)
            .disposed(by: bag)
    }
    
    override func prepareForReuse() {
        self.bag = DisposeBag()
    }
}
