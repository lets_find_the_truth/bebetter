//
//  RealmViewController.swift
//  BeBetter
//
//  Created by Kyryl Nevedrov on 29/11/2019.
//  Copyright © 2019 Kyryl Nevedrov. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
import RxRealm

class RealmViewController: UIViewController {
    // MARK: - @IBOutlets
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var addBookBatButtonItem: UIBarButtonItem!
    
    // MARK: - Properties
    var viewModel: RealmViewModel! = RealmViewModel(useCase: BookUseCase())
    private let bag = DisposeBag()
    
    // MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.configureInputs()
        self.configureOutputs()
    }
    
    // MARK: - Configure Inputs
    private func configureInputs() {
        let output = self.viewModel.output
        output.booksChangeSet
            .drive(onNext: { [weak self] changeSet in
                guard let changeSet = changeSet else {
                    self?.tableView.reloadData()
                    return
                }
                self?.tableView.applyChangeset(changeSet)
            })
            .disposed(by: bag)
    }

    // MARK: - Configure Outputs
    private func configureOutputs() {
        let input = self.viewModel.input
        input.loadBooks.accept(())
        self.addBookBatButtonItem.rx.tap
            .bind(to: input.addBook)
            .disposed(by: bag)
    }
}

extension RealmViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.viewModel.output.books.value.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: RealmTableViewCell.reuseIdentifier) as! RealmTableViewCell
        let book = self.viewModel.output.books.value[indexPath.row]
        let viewModel = RealmTableViewCellViewModel(useCase: self.viewModel.useCase, book: book)
        cell.configureWith(viewModel: viewModel)
        return cell
    }
}

extension RealmViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let book = self.viewModel.output.books.value[indexPath.row]
        self.viewModel.input.deleteBook.accept(book)
    }
}

extension UITableView {
    func applyChangeset(_ changes: RealmChangeset) {
        beginUpdates()
        deleteRows(at: changes.deleted.map { IndexPath(row: $0, section: 0) }, with: .automatic)
        insertRows(at: changes.inserted.map { IndexPath(row: $0, section: 0) }, with: .automatic)
        reloadRows(at: changes.updated.map { IndexPath(row: $0, section: 0) }, with: .automatic)
        endUpdates()
    }
}
