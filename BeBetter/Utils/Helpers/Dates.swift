//
//  Dates.swift
//  BeBetter
//
//  Created by Kyryl Nevedrov on 07/10/2019.
//  Copyright © 2019 Kyryl Nevedrov. All rights reserved.
//

import Foundation

extension DateFormatter {
    enum DateFormat: String {
        // Monday, Sep 17, 2018    EEEE, MMM d, yyyy
        case eeeMMMdyyyy = "EEEE, MMM d, yyyy"
        // 09/17/2018   MM/dd/yyyy
        case mmddyyyy = "MM/dd/yyyy"
        // 09-17-2018 07:32  MM-dd-yyyy HH:mm
        case mmddyyyyhhmm = "MM-dd-yyyy HH:mm"
        // Sep 17, 7:32 AM MMM d, h:mm a
        case mmmdhmma = "MMM d, h:mm a"
        // September 2018   MMMM yyyy
        case mmmyyyy = "MMMM yyyy"
        // Sep 17, 2018   MMM d, yyyy
        case mmmdyyyy = "MMM d, yyyy"
        // Mon, 17 Sep 2018 07:32:32 +0000  E, d MMM yyyy HH:mm:ss Z
        case edmmmyyyyHHmmssZ = "E, d MMM yyyy HH:mm:ss Z"
        // 2018-09-17T07:32:32+0000   yyyy-MM-dd'T'HH:mm:ssZ
        case yyyyMMddTHHmmssZ = "yyyy-MM-dd'T'HH:mm:ssZ"
    }
    
    // MARK: - Init
    convenience init(_ format: DateFormat) {
        self.init()
        self.locale = Locale.current
        self.dateFormat = format.rawValue
    }
    
    // MARK: - Methods
    func string(fromOptional date: Date?) -> String? {
        guard let date = date else { return nil }
        return self.string(from: date)
    }

    func date(fromOptional string: String?) -> Date? {
        guard let string = string else { return nil }
        return self.date(from: string)
    }
}

extension Date {
    // MARK: - Methods
    func time(since date: Date) -> String {
        let allComponents: Set<Calendar.Component> = [.minute, .hour, .day, .weekOfYear, .month, .year, .second]
        let components:DateComponents = Calendar.current.dateComponents(allComponents, from: date, to: self)

        let descendingComponents = ["year": components.year  ?? 0,
                                    "month": components.month  ?? 0,
                                    "week": components.weekOfYear  ?? 0,
                                    "day": components.day ?? 0,
                                    "hour": components.hour ?? 0,
                                    "minute": components.minute ?? 0,
                                    "second": components.second ?? 0]
        for (period, timeAgo) in descendingComponents where timeAgo > 0 {
            return "\(timeAgo.of(period)) ago"
        }

        return "Just now"
    }
}

extension Int {
    // MARK: - Methods
    func of(_ name: String) -> String {
        guard self != 1 else { return "\(self) \(name)" }
        return "\(self) \(name)s"
    }
}

// Working with calendar
/*
    Calendar.current.isDateInToday(date)//and isTomorrow, inWeek, isYesterday
    Calendar.current.compare(date, to: otherDate, toGranularity: .day).rawValue //1 -1 0
    Calendar.current.component(.day, from: date)
    Calendar.current.date(byAdding: .day, value: 1, to: date)!
    Calendar.current.date(bySetting: .day, value: 25, of: date)!
*/
