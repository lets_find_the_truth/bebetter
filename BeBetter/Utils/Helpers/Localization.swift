//
//  Localization.swift
//  BeBetter
//
//  Created by Kyryl Nevedrov on 22/10/2019.
//  Copyright © 2019 Kyryl Nevedrov. All rights reserved.
//

import Foundation

enum Localization {

    enum Global: String, LocalizableDelegate {
        case cancel = "dismiss"
        case close
        case youWereHere = "you were here"
    }

    enum NewTable: String, LocalizableDelegate {
        case title

        var table: String? {
            return "NewTable"
        }
    }
}

protocol LocalizableDelegate {
    var rawValue: String { get }
    var table: String? { get }
    var string: String { get }
}

extension LocalizableDelegate {

    //returns a localized value by specified key located in the specified table
    var string: String {
        return Bundle.main.localizedString(forKey: rawValue, value: nil, table: table)
    }

    // file name, where to find the localized key
    // by default is the Localizable.string table
    var table: String? {
        return nil
    }
}
