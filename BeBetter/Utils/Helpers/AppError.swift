//
//  AppError.swift
//  BeBetter
//
//  Created by Kyryl Nevedrov on 02/11/2019.
//  Copyright © 2019 Kyryl Nevedrov. All rights reserved.
//

import Foundation
import Alamofire

enum AppError: LocalizedError {
    case network(error: AFError)
    case unknown(message: String)
    
    var errorDescription: String? {
        switch self {
        case let .network(error):
            return error.errorDescription
        case let .unknown(message):
            return message
        }
    }
}
