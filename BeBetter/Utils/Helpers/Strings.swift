//
//  Strings.swift
//  BeBetter
//
//  Created by Kyryl Nevedrov on 23/10/2019.
//  Copyright © 2019 Kyryl Nevedrov. All rights reserved.
//

import Foundation

//Every instance of Swift’s Character type represents a single extended grapheme cluster. An extended grapheme cluster is a sequence of one or more Unicode scalars
//Each String value has an associated index type, String.Index, which corresponds to the position of each Character in the string.
// - startIndex is the position of the first Character of a String.
// - endIndex is the position after the last character in a String.
// As a result, the endIndex property isn’t a valid argument to a string’s subscript.
// If a String is empty, startIndex and endIndex are equal.
// - index(before:) and index(after:) the indices before and after a given index
// - index(_:offsetBy:) To access an index farther away from the given index.
// - firstIndex(of: "a")
// Modify:
// - insert(_:at:), insert(contentsOf:at:), remove(at:), and removeSubrange(_:)
// - reversed()
// - uppercased(),lowercased()
// Check:
// - hasPrefix(_:), hasSuffix(_:)
class Strings {
    static func printStringIndicies(_ string: String) {
        for index in string.indices {
            print("Character: \(string[index])")
            print("Index: \(index)")
        }
    }
    
    static func shiftStringRight(_ string: String) -> String {
        var shiftedString = string
        shiftedString.insert(shiftedString.popLast()!, at: shiftedString.startIndex)
        return shiftedString
    }
    
    static func shiftStringLeft(_ string: String) -> String {
        let firstCharacter = string[string.startIndex]
        let rightSubstring = string[string.index(after: string.startIndex)..<string.endIndex]
        var shiftedString = String(rightSubstring)
        shiftedString.append(firstCharacter)
        return shiftedString
    }
    
}
