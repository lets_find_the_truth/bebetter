//
//  LocationManager.swift
//  BeBetter
//
//  Created by Kyryl Nevedrov on 05/11/2019.
//  Copyright © 2019 Kyryl Nevedrov. All rights reserved.
//

import CoreLocation
import UIKit
import RxCocoa
import RxSwift

class LocationManager: NSObject {
    // MARK: - Properties
    var currentLocation: BehaviorRelay<CLLocation?> = BehaviorRelay(value: nil)
    lazy private var manager: CLLocationManager = {
        let manager = CLLocationManager()
        manager.desiredAccuracy = kCLLocationAccuracyBest
        manager.delegate = self
        //for background updating
        //manager.distanceFilter = 5
        //manager.allowsBackgroundLocationUpdates = true
        return manager
    }()
    
    // MARK: - start updating location
    func start() {
        switch CLLocationManager.authorizationStatus() {
        case .notDetermined:
            self.manager.requestWhenInUseAuthorization()
        case .restricted, .denied:
            UIApplication.openSettings()
        case .authorizedWhenInUse, .authorizedAlways:
            self.manager.startUpdatingLocation()
        default:
            break
        }
    }
    
    // MARK: - stop updating location
    func stop() {
        self.manager.stopUpdatingLocation()
    }
    
    // MARK: - get placemart for current location
    func placemarkForCurrentLocation() -> Single<CLPlacemark> {
        return Single.create { [weak self] single in
            if let currentLocation = self?.currentLocation.value {
                let geocoder = CLGeocoder()
                geocoder.reverseGeocodeLocation(currentLocation) { (placemarks, error) in
                        if let placemrk = placemarks?[0] {
                            single(.success(placemrk))
                        } else if let error = error {
                            single(.error(error))
                        }
                }
            }
            
            return Disposables.create()
        }
    }
    
    // MARK: - get coordinate for address string
    func getCoordinate(for addressString : String) -> Single<CLLocation> {
        return Single.create { single in
            let geocoder = CLGeocoder()
            geocoder.geocodeAddressString(addressString) { (placemarks, error) in
                if let placemark = placemarks?[0], let location = placemark.location {
                    single(.success(location))
                } else if let error = error {
                    single(.error(error))
                }
            }
            
            return Disposables.create()
        }
    }
}

// MARK: - Location manager extension
extension LocationManager: CLLocationManagerDelegate {
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        self.currentLocation.accept(locations.first)
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        print("fail update location: \(error.localizedDescription)")
    }
    
    func locationManager(_ manager: CLLocationManager,
                         didChangeAuthorization status: CLAuthorizationStatus) {
        switch status {
        case .restricted, .denied:
            break
        case .authorizedWhenInUse:
            manager.startUpdatingLocation()
        case .notDetermined, .authorizedAlways:
            break
        default:
            break
        }
    }
}
