//
//  Files.swift
//  BeBetter
//
//  Created by Kyryl Nevedrov on 06/12/2019.
//  Copyright © 2019 Kyryl Nevedrov. All rights reserved.
//

import Foundation
import RxSwift

final class Files {
    private static let file = "file.log"
    
    static func writeStringToFile(text: String) -> Completable {
        return Completable.create { completable in
            if let dir = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first {
                let path = dir.appendingPathComponent(file)
                do {
                    try text.write(to: path, atomically: false, encoding: String.Encoding.utf8)
                    completable(.completed)
                } catch let error {
                    completable(.error(error))
                }
            } else {
                completable(.error(AppError.unknown(message: "document directory don't exist")))
            }
            
            return Disposables.create()
        }

    }
    
    static func readStringFromFile() -> Single<String> {
        return Single.create { single in
            if let dir = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first {
                let path = dir.appendingPathComponent(file)
                do {
                    let text = try String(contentsOf: path, encoding: String.Encoding.utf8)
                    single(.success(text))
                } catch let error {
                    single(.error(error))
                }
            } else {
                single(.error(AppError.unknown(message: "document directory don't exist")))
            }
            
            return Disposables.create()
        }
    }
    
    //there are 3 main directory
    // 1) Documents - store user generated content
    // 2) Library - store information than user won't see (has Application Support and Caches subdirectories)
    // 3) Temp - store file that don't need to survive between launches
}
