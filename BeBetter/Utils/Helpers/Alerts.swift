//
//  Alerts.swift
//  BeBetter
//
//  Created by Kyryl Nevedrov on 05/12/2019.
//  Copyright © 2019 Kyryl Nevedrov. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

final class Alert {
    let alertController: UIAlertController
    
    init(title: String?, message: String?, preferredStyle: UIAlertController.Style) {
        self.alertController = UIAlertController(title: title, message: message, preferredStyle: preferredStyle)
    }
    
    init(attributedTitle: NSAttributedString?, attributedMessage: NSAttributedString?, preferredStyle: UIAlertController.Style) {
        self.alertController = UIAlertController(title: nil, message: nil, preferredStyle: preferredStyle)
        self.alertController.setValue(attributedTitle, forKey: "attributedTitle")
        self.alertController.setValue(attributedMessage, forKey: "attributedMessage")
    }
    
    func add(action: UIAlertAction) -> Alert {
        self.alertController.addAction(action)
        return self
    }
}

extension UIAlertAction {
    convenience init(title: String, style: UIAlertAction.Style, output: PublishRelay<Void>) {
        self.init(title: title, style: style) { (action) in
            output.accept(())
        }
    }
}
