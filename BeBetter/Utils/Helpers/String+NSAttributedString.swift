//
//  String+NSAttributedString.swift
//  BeBetter
//
//  Created by Kyryl Nevedrov on 23/10/2019.
//  Copyright © 2019 Kyryl Nevedrov. All rights reserved.
//

import UIKit

extension String {
    // attributed string from html
    func attributedStringForHtml() -> NSAttributedString? {
        let data = Data(self.utf8)
        guard let attributedString = try? NSAttributedString(data: data, options: [.documentType: NSAttributedString.DocumentType.html], documentAttributes: nil) else  {
            return nil
        }
        return attributedString
    }
}
