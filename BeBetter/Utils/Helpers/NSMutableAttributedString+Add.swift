//
//  NSAttributedString.swift
//  BeBetter
//
//  Created by Kyryl Nevedrov on 23/10/2019.
//  Copyright © 2019 Kyryl Nevedrov. All rights reserved.
//

import UIKit

extension NSMutableAttributedString {

    // add backgound color
    func add(background color: UIColor, to range: NSRange) {
        let attributes: [NSAttributedString.Key: Any] = [.backgroundColor: color]
        self.addAttributes(attributes, range: range)
    }
    
    // add foreground color
    func add(foreground color: UIColor, to range: NSRange) {
        let attributes: [NSAttributedString.Key: Any] = [.foregroundColor: color]
        self.addAttributes(attributes, range: range)
    }

    // add image
    func add(_ image: UIImage, to range: NSRange? = nil) {
        let imageAttachment = NSTextAttachment()
        imageAttachment.image = image
        let stringWithImage = NSAttributedString(attachment: imageAttachment)
        if let range = range {
            self.replaceCharacters(in: range, with: stringWithImage)
        } else {
            // add image to the end
            self.append(stringWithImage)
        }
    }
    
    // add link
    func add(link: String, to text: String) {
        let range = self.mutableString.range(of: text)
        self.addAttribute(.link, value: link, range: range)
    }
    
    // add attribute to text
    func addAttribute(_ name: NSAttributedString.Key, value: Any, to text: String) {
        let range = self.mutableString.range(of: text)
        self.addAttribute(name, value: value, range: range)
    }
}

