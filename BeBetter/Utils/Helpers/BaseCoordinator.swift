//
//  BaseCoordinator.swift
//  BeBetter
//
//  Created by Kyryl Nevedrov on 18/11/2019.
//  Copyright © 2019 Kyryl Nevedrov. All rights reserved.
//

import RxSwift
import Foundation

protocol Coordinator: class {
    associatedtype ResultType
    var identifier: UUID { get }
    var childCoordinators: [UUID: Any] { get set }
    func start() -> Observable<ResultType>
    func start(with option: DeepLinkOption?) -> Observable<ResultType>
    func coordinate<T>(to coordinator: BaseCoordinator<T>, deepLink: DeepLinkOption?) -> Observable<T>
}

extension Coordinator {
    func start() -> Observable<ResultType> {
        start(with: nil)
    }
}

class BaseCoordinator<ResultType>: Coordinator {
    var childCoordinators: [UUID : Any] = [:]
    var identifier: UUID = UUID()
    
    func start(with option: DeepLinkOption?) -> Observable<ResultType> {
        fatalError("Start function must be implemented")
    }
    
    func coordinate<T>(to coordinator: BaseCoordinator<T>, deepLink: DeepLinkOption? = nil) -> Observable<T> {
        childCoordinators[coordinator.identifier] = coordinator
        return coordinator.start(with: deepLink)
            .do(onNext: { [weak self] (next) in
                self?.childCoordinators[coordinator.identifier] = nil
            }, onCompleted: { [weak self] in
                self?.childCoordinators[coordinator.identifier] = nil
            })
    }
}

// MARK: - Deep links option
enum DeepLinkOption: String {
    case customUI
    case settings
    case signUp

    static func build(with userActivity: NSUserActivity) -> DeepLinkOption? {
        guard userActivity.activityType == NSUserActivityTypeBrowsingWeb,
            let url = userActivity.webpageURL,
            let components = URLComponents(url: url, resolvingAgainstBaseURL: true),
            let queryItems = components.queryItems else {
                return nil
        }

        var queryParams = [String: String]()
        for queryItem in queryItems {
            queryParams[queryItem.name] = queryItem.value
        }
        
        guard let id = queryParams["link_id"] else { return nil }
        return self.getDeepLink(from: id)
    }
  
    static func build(with dict: [String : Any]?) -> DeepLinkOption? {
        guard let id = dict?["link_id"] as? String else { return nil }
        return self.getDeepLink(from: id)
    }
    
    private static func getDeepLink(from id: String) -> DeepLinkOption? {
        switch id {
        case customUI.rawValue: return .customUI
        case settings.rawValue: return .settings
        case signUp.rawValue: return .signUp
        default: return nil
        }
    }
}
