//
//  UIApplication+OpenUrl.swift
//  BeBetter
//
//  Created by Kyryl Nevedrov on 24/10/2019.
//  Copyright © 2019 Kyryl Nevedrov. All rights reserved.
//

import UIKit

// all url shemes at https://ios.gadgethacks.com/news/always-updated-list-ios-app-url-scheme-names-0184033/
extension UIApplication {
    
    // MARK: - open url
    static func open(url: URL?) {
        guard let url = url, UIApplication.shared.canOpenURL(url) else {
            return
        }
        UIApplication.shared.open(url, options: [:], completionHandler: nil)
    }
    
    // MARK: - open settings
    static func openSettings() {
        let settingsUrl = URL(string: UIApplication.openSettingsURLString)
        guard let url = settingsUrl, UIApplication.shared.canOpenURL(url) else {
            return
        }
        UIApplication.shared.open(url, options: [:], completionHandler: nil)
    }
}
