//
//  MapService.swift
//  BeBetter
//
//  Created by Kyryl Nevedrov on 06/11/2019.
//  Copyright © 2019 Kyryl Nevedrov. All rights reserved.
//

import Foundation
import Alamofire
import MapKit

enum MapService: URLRequestModel {
    case getRoutes(source: CLLocationCoordinate2D, destination: CLLocationCoordinate2D)

    // MARK: - Path
    internal var path: String {
        switch self {
        case .getRoutes:
            return "/directions/json"
        }
    }

    // MARK: - Parameters
    internal var parameters: Parameters? {
        switch self {
        case .getRoutes(let source, let destionation):
            return [
                "origin": "\(source.latitude),\(source.longitude)",
                "destination": "\(destionation.latitude),\(destionation.longitude)",
                "key": "AIzaSyBlgoIQhuXQcMt2L6Cq2wdvWcJVgonq7NA"
            ]
        }
    }
    
    // MARK: - Methods
    internal var method: HTTPMethod {
        switch self {
        case .getRoutes:
            return .get
        }
    }
    
    internal var successCodes: [Int] {
        switch self {
        case .getRoutes:
            return [200]
        }
    }
    
    internal var mainURL: URL {
        return URL(string: "https://maps.googleapis.com/maps/api")!
    }
}
