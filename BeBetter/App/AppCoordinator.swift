//
//  AppCoordinator.swift
//  BeBetter
//
//  Created by Kyryl Nevedrov on 18/11/2019.
//  Copyright © 2019 Kyryl Nevedrov. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

class AppCoordinator: BaseCoordinator<Void> {
    // MARK: - Properties
    let signedIn = PublishRelay<String>()
    let signedOut = PublishRelay<Void>()
    private let window: UIWindow
    private let disposeBag = DisposeBag()
    private let navigationController: UINavigationController = UINavigationController()
    
    // MARK: - Init
    init(window: UIWindow) {
        self.window = window
        self.window.makeKeyAndVisible()
        self.window.rootViewController = navigationController
    }
    
    // MARK: - start method
    override func start(with option: DeepLinkOption?) -> Observable<Void> {
        self.childCoordinators.removeAll()
        if let deepLink = option {
            switch deepLink {
            case .settings:
                UIApplication.openSettings()
            default:
                self.showLogin()
            }
        } else {
            self.showHome(with: "Tom")
        }
        self.signedOut.bind { [weak self] in self?.showLogin() }.disposed(by: disposeBag)
        self.signedIn.bind { [weak self] name in self?.showHome(with: name) }.disposed(by: disposeBag)
        return Observable.never()
    }
    
    // MARK: - Methods
    private func showLogin()   {
        let _ = coordinate(to: AuthCoordinator(navigationController: navigationController))
    }
    
    private func showHome(with name: String)  {
        let _ = coordinate(to: HomeCoordinator(name: name, navigationController: navigationController))
    }
}
